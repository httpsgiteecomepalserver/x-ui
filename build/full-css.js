import { readFileSync, writeFileSync } from 'fs'
import { ensureDirSync } from 'fs-extra'
import { join } from 'path'
import sass from 'sass'

const cwd = process.cwd()

const light = join(cwd, 'components/style/default.scss')
const dark = join(cwd, 'components/style/dark.scss')

ensureDirSync(join(cwd, 'theme'))

writeFileSync(join(cwd, 'theme/light.css'), sass.compile(light).css)
writeFileSync(join(cwd, 'theme/dark.css'), sass.compile(dark).css)
