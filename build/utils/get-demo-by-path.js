import fs from 'fs-extra'
import demoLoader from '../loaders/demo-loader'
import docLoader from '../loaders/doc-loader'

export default async function(path) {
  const code = await fs.readFile(path, 'utf-8')
  if (path.endsWith('.demo.md')) {
    return demoLoader(code, path)
  } else if (path.endsWith('.md')) {
    return docLoader(code, path)
  }
}