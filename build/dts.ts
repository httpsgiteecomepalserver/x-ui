import { join } from 'path'
import { defineConfig, PluginOption } from 'vite'
import vue from '@vitejs/plugin-vue'
import dts from 'vite-plugin-dts'

export default defineConfig({
  build: {
    outDir: join(process.cwd(), 'types'),
    lib: {
      formats: ['es'],
      entry: join(process.cwd(), 'components')
    }
  },
  plugins: [
    vue(),
    dts({
      include: '**/components',
      compilerOptions: {
        emitDeclarationOnly: true
      }
    }),
    {
      // todo
      name: 'xxxxxxxxxx',
      generateBundle(options, bundle) {
        console.log('==================================================')
        console.log(Object.keys(bundle).join('\n'))
        console.log('==================================================')

        Object.keys(bundle).forEach(k => {
          if (!k.endsWith('.d.ts')) delete bundle[k]
        })
      }
    } as PluginOption
  ]
})
