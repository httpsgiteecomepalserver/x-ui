import { rollup } from 'rollup'
import vue from '@vitejs/plugin-vue'
import vueJsx from '@vitejs/plugin-vue-jsx'
import VueMacros from 'unplugin-vue-macros/rollup'
import esbuild from 'rollup-plugin-esbuild'
import fg from 'fast-glob'
import pkg from '../package.json' assert { type: 'json' }
import path from 'path'

import vuecss from './plugins/vue-css/index.js'
import scss from './plugins/preserve-scss/index.js'
import rename from './plugins/rollup-plugin-rename/index.js'

async function build() {
  const bundle = await rollup({
    input: await fg('components/**/*.{ts,tsx,js,jsx,vue}', { ignore: ['**/demos'] }),
    plugins: [
      // nodeResolve(),
      ...VueMacros({
        setupSFC: false,
        setupComponent: false,
        plugins: {
          vue: vue(),
          vueJsx: vueJsx()
        }
      }),
      esbuild(),
      vuecss(),
      scss(),
      rename({ include: '**', map: name => name.replace('.scss.js', '.css') })
    ],
    external: Object.keys({ ...pkg.dependencies, ...pkg.peerDependencies }),
    treeshake: false
  })

  const formats = ['esm', 'cjs']

  const ps = formats.map(format => {
    bundle.write({
      format,
      dir: path.join(process.cwd(), format),
      preserveModules: true
    })
  })

  await Promise.all(ps)
}

build()
