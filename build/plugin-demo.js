const demoLoader = require('./loaders/demo-loader')
const docLoader = require('./loaders/doc-loader')

module.exports = function(source) {
  const path = this.resourcePath
  if (path.endsWith('.demo.md')) {
    return demoLoader(source, path)
  } else if (path.endsWith('.md')) {
    return docLoader(source, path)
  }
}