import css_wrap from 'css-wrap'
import { copyFileSync, readFileSync, writeFileSync } from 'fs'
import { join } from 'path'
import postcss from 'postcss'

const cwd = process.cwd()

const light = join(cwd, 'theme/light.css')
const dark = join(cwd, 'theme/dark.css')

const lightOut = join(cwd, 'examples/styles/theme/light.css')
const darkOut = join(cwd, 'examples/styles/theme/dark.css')

copyFileSync(light, lightOut)

const css = readFileSync(dark, { encoding: 'utf-8' })
writeFileSync(darkOut, css_wrap(css, { selector: '.dark' }), { encoding: 'utf-8' })
