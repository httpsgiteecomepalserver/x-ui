import fse from 'fs-extra'
import fs from 'fs'
import path from 'path'
import * as marked from 'marked'
import camelCase from 'camelcase'
import mdRender from './md-render'

const projectPath = path.resolve(__dirname, '..', '..')

function resolveDemoTitle(fileName, demoEntryPath) {
  const demoStr = fs.readFileSync(path.resolve(demoEntryPath, '..', fileName)).toString()
  return demoStr.match(/# ([^\n]+)/)[1]
}

function resolveDemoInfos(tokenText, url, env) {
  const ids = tokenText
    .split('\n')
    .map(line => line.trim())
    .filter(id => id.length)
  const infos = []
  const exist = e => fs.existsSync(path.resolve(url, '..', e))
  for (const id of ids) {
    const debug = id.toLowerCase().includes('debug')
    if (env === 'production' && debug) continue

    const fileName = [`${id}.demo.vue`, `${id}.demo.md`].find(exist)
    const variable = `${camelCase(id)}Demo`
    infos.push({
      id,
      variable,
      fileName,
      title: resolveDemoTitle(fileName, url),
      tag: `<${variable} />`,
      debug
    })
  }
  return infos
}

function genDemosTemplate(demoInfos, colSpan) {
  const demos = demoInfos.map(e => e.tag).join('')
  return `<component-demos :span="${colSpan}">${demos}</component-demos>`
}

function genScript(demoInfos, components = [], url, forceShowAnchor) {
  const importStmts = demoInfos
    .map(({ variable, fileName }) => `import ${variable} from './${fileName}'`)
    .concat(components.map(({ importStmt }) => importStmt))
    .join('\n')
  const componentStmts = demoInfos
    .map(({ variable }) => variable)
    .concat(components.map(({ ids }) => ids).flat())
    .join(',')
  const script = `
<script>
${importStmts}
export default {
  components: {${componentStmts}}
}
</script>
`
  return script
}

export default function (text, path, env = 'development') {
  const colSpan = ~text.search('<!--single-column-->') ? 1 : 2
  const tokens = marked.lexer(text)

  const isDemos = token => token.type === 'code' && token.lang === 'demo'

  const anchors = tokens
    .map(e => {
      if (isDemos(e)) return 'demos'
      if (e.type === 'heading' && e.depth > 1) {
        return { title: e.text, id: e.text.replace(/ /g, '-') }
      }
    })
    .filter(e => e)

  // resolve demos
  const demosIndex = tokens.findIndex(isDemos)
  let demoInfos = []
  if (~demosIndex) {
    demoInfos = resolveDemoInfos(tokens[demosIndex].text, path, env)
    tokens.splice(demosIndex, 1, {
      type: 'html',
      pre: false,
      text: genDemosTemplate(demoInfos, colSpan)
    })
  }
  const docMainTemplate = marked.parser(tokens, {
    gfm: true,
    renderer: mdRender
  })

  const demosI = anchors.findIndex(e => e == 'demos')
  anchors.splice(demosI, 1, ...demoInfos.map(e => ({ title: e.title, id: 'component-' + e.id })))

  const docTemplate = `
<template>
  <doc :anchors='${JSON.stringify(anchors)}'>
    ${docMainTemplate}
  </doc>
</template>`
  const docScript = genScript(demoInfos, [], path)
  return `${docTemplate}\n\n${docScript}`
}
