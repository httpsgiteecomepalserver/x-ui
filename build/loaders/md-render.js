import hljs from 'highlight.js'
import * as marked from 'marked'

const renderer = new marked.Renderer()

const overrides = {
//   table(header, body) {
//     header = JSON.parse(header.slice(0, -2))
//     body = JSON.parse(`[${body.slice(0 ,-2)}]`).map((tr, ri) => {
//       const row = {}
//       tr.forEach((td, i) => row[header[i].key] = td)
//       return row
//     })
//     return `
// <x-table :columns='${JSON.stringify(header)}' :data='${JSON.stringify(body)}'>
//   ${header.map(col => (
//     `<template #${col.key}="{ row, index }"><v-runtime-template :template="row.${col.key}" /></template>`
//   )).join('\n')}
// </x-table>
// `
//   },
//   tablerow(content) {
//     return `[${content.slice(0, -2)}], `
//   },
//   tablecell(content, flags) {
//     content = JSON.stringify(content)
//     if (flags.header) {
//       return `{ "title": ${content}, "key": ${content} }, `
//     } else {
//       return `${content}, `
//     }
//   },
  table(header, body) {
    if (body) body = '<tbody>' + body + '</tbody>\n'
    header = '<thead>' + header + '</thead>\n'
    return (
      '<x-simple-table class="md-table" v-border-light="{borderGradientSize: 160, bgColor: null}">\n' +
      header +
      body +
      '</x-simple-table>\n'
    )
  },
  tablerow(content) {
    return '<tr>\n' + content + '</tr>\n'
  },
  tablecell(content, flags) {
    const type = flags.header ? 'th' : 'td'
    const tag = flags.align
      ? '<' + type + ' align="' + flags.align + '">'
      : '<' + type + '>'
    return tag + content + '</' + type + '>\n'
  },
  code(code, language) {
    if (language.startsWith('__')) language = language.replace('__', '')
    const isLanguageValid = !!(language && hljs.getLanguage(language))
    if (!isLanguageValid) throw new Error(`MdRendererError: ${language} is not valid for code - ${code}`)
    
    const highlighted = hljs.highlight(code, { language }).value
    return `<x-code language='${language}'><pre v-pre>${highlighted}</pre></x-code>`
  },
  heading(text, level) {
    const id = text.replace(/ /g, '-')
    return `<h${level} id="${id}" class="x-h${level}">${text}</h${level}>`
  },
  codespan(code) {
    return `<x-tag v-border-light="{bgColor: null}" size="medium">${code}</x-tag>`
  },
  blockquote(quote) {
    return `<x-blockquote>${quote}</x-blockquote>`
  },
  paragraph(text) {
    return `<p class='x-p'>${text}</p>`
  },
  link(href, title, text) {
    let target = ''
    if (/^(http:|https:)/.test(href)) target = '_blank'
    return `<x-a href="${href}" target="${target}">${text}</x-a>`
  },
  list(body, ordered, start) {
    const type = ordered ? 'ol' : 'ul'
    return `<${type} class='x-${type}'>\n` + body + `</${type}>\n`
  }
}

Object.assign(renderer, overrides)

export default renderer