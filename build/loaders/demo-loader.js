import * as marked from 'marked'
import mdRender from './md-render'
import fs from 'fs'
import path from 'path'

const projectPath = path.resolve(__dirname, '..', '..')

const demoBlock = fs
  .readFileSync(path.resolve(__dirname, 'ComponentDemoTemplate.vue'))
  .toString()

function getPartsOfDemo (tokens) {
  let template = null
  let script = null
  let style = null
  let title = null
  const descTokens = []
  descTokens.links = tokens.links
  for (const token of tokens) {
    if (token.type === 'heading' && token.depth === 1) {
      title = token.text
    } else if (
      token.type === 'code' &&
      (token.lang === 'template' || token.lang === 'html')
    ) {
      template = token.text
    } else if (
      token.type === 'code' &&
      (token.lang === 'script' || token.lang === 'js')
    ) {
      script = token.text
    } else if (
      token.type === 'code' &&
      (token.lang === 'style' || token.lang === 'css')
    ) {
      style = token.text
    } else {
      descTokens.push(token)
    }
  }
  return {
    template,
    script,
    style,
    title,
    desc: marked.parser(descTokens, {
      renderer: mdRender
    })
  }
}

function mergeParts (parts) {
  const mergedParts = {
    ...parts
  }
  mergedParts.title = parts.title
  mergedParts.desc = parts.desc
  mergedParts.code = ''
  if (parts.template) {
    mergedParts.code += `<template>\n${parts.template
      .split('\n')
      .map((line) => (line.length ? '  ' + line : line))
      .join('\n')}\n</template>`
  }
  if (parts.script) {
    if (parts.template) mergedParts.code += '\n\n'
    mergedParts.code += `<script>
${parts.script}
</script>`
  }
  if (parts.style) {
    if (parts.template || parts.script) mergedParts.code += '\n\n'
    mergedParts.code += `<style scoped>
${parts.style}
</style>`
  }
  mergedParts.code = Buffer.from(mergedParts.code).toString('base64')
  return mergedParts
}

const cssRuleRegex = /([^{}]*)(\{[^}]*\})/g

function genStyle (sourceStyle) {
  return '<style scoped>\n' + sourceStyle + '</style>'
}

function genVueComponent (parts, fileName, { resourcePath, relativeUrl}, noRunning = false) {
  const demoFileNameReg = /<!--DEMO_FILE_NAME-->/g
  const relativeUrlReg = /<!--URL-->/g
  const compNameReg = /<!--COMPONENT_NAME-->/
  const titleReg = /<!--TITLE_SLOT-->/g
  const descReg = /<!--DESC_SLOT-->/
  const codeReg = /<!--CODE_SLOT-->/
  const scriptReg = /<!--SCRIPT_SLOT-->/
  const styleReg = /<!--STYLE_SLOT-->/
  const demoReg = /<!--DEMO_SLOT-->/
  let src = demoBlock
  src = src.replace(demoFileNameReg, fileName)
  src = src.replace(relativeUrlReg, relativeUrl)
  src = src.replace(compNameReg, getCompName(resourcePath))
  if (parts.desc) {
    src = src.replace(descReg, parts.desc)
  }
  if (parts.title) {
    src = src.replace(titleReg, parts.title)
  }
  if (parts.code) {
    src = src.replace(codeReg, parts.code)
  }
  if (parts.script && !noRunning) {
    src = src.replace(scriptReg, '<script>\n' + parts.script + '\n</script>')
  }
  if (parts.style) {
    const style = genStyle(parts.style)
    if (style !== null) {
      src = src.replace(styleReg, style)
    }
  }
  if (parts.template) {
    src = src.replace(demoReg, parts.template)
  }
  return src.trim()
}

function getFileName (resourcePath) {
  const fileNameWithExtension = path.basename(resourcePath)
  return [fileNameWithExtension.split('.')[0], fileNameWithExtension]
}

function getCompName (resourcePath) {
  const compPath = path.resolve(resourcePath, '..', '..', '..')
  return path.basename(compPath).split('.')[0]
}

function convertMd2Demo (text, { resourcePath, relativeUrl }) {
  const noRunning = /<!--no-running-->/.test(text)
  const tokens = marked.lexer(text)
  const parts = getPartsOfDemo(tokens)
  const mergedParts = mergeParts(parts)
  const [fileName] = getFileName(resourcePath)
  const vueComponent = genVueComponent(
    mergedParts,
    fileName,
    { resourcePath, relativeUrl },
    noRunning
  )
  return vueComponent
}

export default function(content, path) {
  const relativeUrl = path.replace(projectPath + '\\', '')
  return convertMd2Demo(content, {
    relativeUrl,
    resourcePath: path
  })
}