import vue from '@vitejs/plugin-vue'
import vueJsx from '@vitejs/plugin-vue-jsx'
import getDemoByPath from './utils/get-demo-by-path'

const mdReg = /(\.md)$/

const vuePlugin = vue({ include: [/\.vue$/, /\.md$/] })

const mdPlugin = {
  name: 'vite-plugin-md',
  transform(code, path) {
    if (!mdReg.test(path)) return
    return getDemoByPath(path)
  },
  async handleHotUpdate(ctx) {
    if (!mdReg.test(ctx.file)) return
    const code = await getDemoByPath(ctx.file)
    return vuePlugin.handleHotUpdate({
      ...ctx,
      read: () => code
    })
  }
}

export default () => [mdPlugin, vuePlugin, vueJsx()]