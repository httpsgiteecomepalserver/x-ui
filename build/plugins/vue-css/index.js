import { existsSync, mkdirSync, writeFileSync, rm, rmSync } from 'fs'
import { ensureFileSync } from 'fs-extra'
import path from 'path'
import sass from 'sass'

export default function () {
  const vuestyles = []

  return {
    name: 'vue-css',
    transform(code, id) {
      if (!id.split('?')[1]?.startsWith('vue&type=style')) return
      const _id = id.split('?')[0] + '.css'
      const exist = vuestyles.find(e => e.id == _id)
      const lang = id.match(/lang[=\.](\w+)/)[1]
      if (exist) exist.code = code + '\n'
      else vuestyles.push({ id, _id, code, lang })
    },
    generateBundle(options, bundle) {
      // 处理 vue style
      Object.values(vuestyles).map(style => {
        const importerId = this.getModuleInfo(style.id).importers[0]
        const importer = Object.values(bundle).find(e => e.facadeModuleId == importerId)
        importer.code += `\nimport './${path.basename(importer.fileName)}.css';\n\n`

        const styleFilename = path.basename(importer.fileName + '.' + style.lang)
        const temp = path.join(importer.facadeModuleId, '..', styleFilename)
        writeFileSync(temp, style.code, { encoding: 'utf-8' })

        const output = path.resolve(process.cwd(), options.dir, importer.fileName + '.css')
        ensureFileSync(output)
        const css = sass.compile(temp).css
        writeFileSync(output, css, { encoding: 'utf-8' })

        rmSync(temp)
      })
    }
  }
}
