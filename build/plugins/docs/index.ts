import type { PluginOption } from 'vite'
import path from 'path'
import { Lexer, Parser, marked } from 'marked'
import mdRender from '../../loaders/md-render'
import fetchCode, { reObj } from '../../../scripts/utils/fetchCode'
import ts2js from '../utils/ts2js'

import { ComponentDemoProps } from '../../../examples/utils/ComponentDemo.vue'

const projectPath = path.resolve(__dirname, '..', '..').replace(/\\/g, '/')

export default function () {
  return {
    transform(code, id) {
      if (!id.endsWith('.demo.vue')) return

      const info = {} as ComponentDemoProps

      const md = fetchCode(code, 'markdownContent')
      const tokens = new Lexer().lex(md)
      const titleFn = (e: marked.Token) => e.type === 'heading' && e.depth === 1
      // @ts-ignore
      info.title = tokens.find(titleFn)?.text
      const descTokens = tokens.filter(e => !titleFn(e))
      const desc = new Parser({ renderer: mdRender }).parse(descTokens)

      code = code.replace(reObj.markdown, '').trim()

      const template = fetchCode(code, 'template')
      const script = fetchCode(code, 'script')
      const style = fetchCode(code, 'style')

      const scriptAttributes = script.match(/<script(.*?)>/)?.[1] ?? ''
      const isSetup = scriptAttributes.includes('setup')
      const isTs = scriptAttributes.includes('ts')

      let jsCode = ''
      if (isTs) {
        jsCode = ts2js(fetchCode(code, 'scriptContent'))
        jsCode = `<script${isSetup ? ' setup' : ''}>\n${jsCode}\n</script>`
        jsCode = `${template}\n\n${jsCode}\n\n${style}`
      }

      info.code = Buffer.from(code).toString('base64')
      info.jsCode = Buffer.from(jsCode).toString('base64')

      info.relativeUrl = id.replace(projectPath + '/', '')
      info.componentName = getCompName(id)
      info.demoFileName = path.basename(id).split('.')[0]

      const newContent = `
<template>
  <component-demo v-bind='${JSON.stringify(info)}'>
  ${template.replace('<template>', '<template #demo>')}
  <template #description>${desc}</template>
  </component-demo>
</template>
${script}
${style}
`
      // return {
      //   code: newContent,
      //   map: null
      // }
      return newContent
    }
  } as PluginOption
}

function getCompName(resourcePath) {
  const compPath = path.resolve(resourcePath, '..', '..', '..')
  return path.basename(compPath).split('.')[0]
}
