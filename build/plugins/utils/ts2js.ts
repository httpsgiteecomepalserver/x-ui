import { transformSync } from 'esbuild'

export default function ts2js(code: string) {
  // esbuild will remove blank line
  code = code.replace(/\n(\s)*\n/g, '\n__blankline\n')

  code = transformSync(code, {
    loader: 'ts',
    minify: false,
    minifyWhitespace: false,
    treeShaking: false
  }).code

  return code.trim().replace(/__blankline;/g, '')
}
