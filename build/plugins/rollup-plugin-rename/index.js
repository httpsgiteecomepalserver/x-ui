import { createFilter } from '@rollup/pluginutils'
import { walk } from 'estree-walker'
import MagicString from 'magic-string'

var NodeType = /* @__PURE__ */ (NodeType2 => {
  NodeType2['Literal'] = 'Literal'
  NodeType2['CallExpression'] = 'CallExpression'
  NodeType2['Identifier'] = 'Identifier'
  NodeType2['ImportDeclaration'] = 'ImportDeclaration'
  NodeType2['ExportNamedDeclaration'] = 'ExportNamedDeclaration'
  NodeType2['ExportAllDeclaration'] = 'ExportAllDeclaration'
  return NodeType2
})(NodeType || {})

export function isEmpty(array) {
  return !array || array.length === 0
}

export function getRequireSource(node) {
  if (node.type !== 'CallExpression' /* CallExpression */) {
    return false
  }
  if (node.callee.type !== 'Identifier' /* Identifier */ || isEmpty(node.arguments)) {
    return false
  }
  const args = node.arguments
  if (node.callee.name !== 'require' || args[0].type !== 'Literal' /* Literal */) {
    return false
  }
  return args[0]
}

export function getImportSource(node) {
  if (node.type !== 'ImportDeclaration' /* ImportDeclaration */ || node.source.type !== 'Literal' /* Literal */) {
    return false
  }
  return node.source
}

export function getExportSource(node) {
  const exportNodes = ['ExportAllDeclaration' /* ExportAllDeclaration */, 'ExportNamedDeclaration' /* ExportNamedDeclaration */]
  if (!exportNodes.includes(node.type) || !node.source || node.source.type !== 'Literal' /* Literal */) {
    return false
  }
  return node.source
}

export function rewrite(input, map) {
  return map(input)
}

export default function rename(options) {
  const filter = createFilter(options.include, options.exclude)
  const sourceMaps = options.sourceMap !== false
  return {
    name: 'rename-rollup',
    generateBundle(_, bundle) {
      const files = Object.entries(bundle)
      for (const [key, file] of files) {
        if (!filter(file.facadeModuleId)) {
          continue
        }
        file.facadeModuleId = rewrite(file.facadeModuleId, options.map) || file.facadeModuleId
        file.fileName = rewrite(file.fileName, options.map) || file.fileName
        file.imports.map(imported => {
          if (!filter(imported)) {
            return imported
          }
          return rewrite(imported, options.map) || imported
        })
        if (file.code) {
          const magicString = new MagicString(file.code)
          let ast
          try {
            ast = this.parse(file.code, { ecmaVersion: 6, sourceType: 'module' })
          } catch (e) {
            continue
          }
          walk(ast, {
            enter(node) {
              if (['ImportDeclaration' /* ImportDeclaration */, 'CallExpression' /* CallExpression */, 'ExportAllDeclaration' /* ExportAllDeclaration */, 'ExportNamedDeclaration' /* ExportNamedDeclaration */].includes(node.type)) {
                const req = getRequireSource(node) || getImportSource(node) || getExportSource(node)
                if (req) {
                  const { start, end } = req
                  const newPath = rewrite(req.value, options.map)
                  magicString.overwrite(start, end, `'${newPath}'`)
                }
              }
            }
          })
          if (sourceMaps) {
            file.map = magicString.generateMap()
          }
          file.code = magicString.toString()
        }
        delete bundle[key]
        bundle[rewrite(key, options.map) || key] = file
      }
    }
  }
}
