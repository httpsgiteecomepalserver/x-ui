import sass from 'sass'

/**
 * @returns {import('rollup').Plugin}
 */
export default function scss() {
  return {
    name: 'scss',
    transform: (code, id) => (id.includes('scss') ? '' : null),
    generateBundle(options, bundle) {
      Object.values(bundle)
        .filter(e => e.fileName.includes('.scss'))
        .forEach(e => (e.code = sass.compile(e.facadeModuleId).css))
    }
  }
}
