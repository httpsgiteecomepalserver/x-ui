const cdns = [
  {
    module: 'vue',
    global: 'Vue',
    entry: 'https://lib.baomitu.com/vue/3.2.8/vue.global.min.js'
  },
  {
    module: 'vue-router',
    global: 'VueRouter',
    entry: 'https://lib.baomitu.com/vue-router/4.0.11/vue-router.global.min.js'
  },
  {
    module: 'moment',
    global: 'moment',
    entry: 'https://lib.baomitu.com/moment.js/2.29.1/moment.min.js'
  },
  {
    module: 'highlight.js',
    global: 'hljs',
    entry: 'https://lib.baomitu.com/highlight.js/11.5.0/highlight.min.js'
  }
]

export default cdns
