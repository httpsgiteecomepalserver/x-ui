/**
 * @returns {import('unplugin-vue-components/types').ComponentResolver[]}
 */
function resolver() {
  return [
    {
      type: 'component',
      resolve: name => {
        if (!/^X[A-Z]/.test(name)) return

        const kebabCase = s => s.replace(/[A-Z]/g, val => `-${val.toLowerCase()}`).replace(/^\-/, '')
        const importName = name.slice(1)
        const partialName = kebabCase(importName)

        const styleDirMap = {
          InputSearch: 'input'
        }
        const styleDir = styleDirMap[importName] ?? partialName

        const sideEffects = !['a', 'simple-table'].includes(partialName) ? `x-ui-vue3/esm/${styleDir}/style` : undefined

        return { name: importName, from: 'x-ui-vue3', sideEffects }
      }
    },
    {
      type: 'directive',
      resolve: name => {
        const directives = {
          Skeleton: { dir: 'skeleton' },
          SkeletonItem: { dir: 'skeleton' },
          Acrylic: { dir: 'acrylic' },
          Sunken: { dir: 'sunken' },
          BorderLight: { dir: 'border-light' }
        }

        const directive = directives[name]
        if (!directive) return

        return { name, from: 'x-ui-vue3', sideEffects: `x-ui-vue3/${directive.dir}/style/index.js` }
      }
    }
  ]
}

export default resolver
