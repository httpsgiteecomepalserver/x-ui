import { defineComponent } from 'vue'

const prefixCls = 'component-demos'

const ComponentDemos = defineComponent({
  name: prefixCls,
  props: {
    span: { type: Number, default: 2 }
  },
  setup(props, { slots }) {
    return () => {
      const { span } = props
      const items = slots.default?.() || []

      const gridTemplateColumns = 'minmax(0, 1fr) '.repeat(span)
      
      return (
        <div class={prefixCls} style={{ gridTemplateColumns }}>
          {
            span === 1
              ? items
              : (
                [...Array(span)].map((_, i) => (
                  <ComponentDemos span={1}>{ items.filter((_, ii) => ii % span == i) }</ComponentDemos>
                ))
              )
          }
        </div>
      )
    }
  }
})

export default ComponentDemos