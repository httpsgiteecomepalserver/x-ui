import { h, computed } from 'vue'
import { useColorMode, usePreferredDark } from '@vueuse/core'
import moment from 'moment'
import { Notification } from 'x-ui-vue3'

const preferredDark = usePreferredDark()
export const colorMode = useColorMode({ emitAuto: true })

export const isDark = computed({
  get() {
    return colorMode.value === 'auto' ? preferredDark.value : colorMode.value == 'dark'
  },
  set(v) {
    colorMode.value = v ? 'dark' : 'light'
  }
})


// 主题切换 通知
setTimeout(() => {
  const img = !isDark.value
    ? 'https://img.zcool.cn/community/018a875cb7d985a801208f8b899311.jpg@1280w_1l_2o_100sh.jpg'
    : 'https://img.zcool.cn/community/01e3445cb7d964a801214168671d3b.jpg@1280w_1l_2o_100sh.jpg'
  const text = !isDark.value
    ? '夜间模式'
    : '明亮模式'
  const hide = Notification.create({
    cover: h('div', { style: `height: 200px; background: url(${img}) center/cover no-repeat;` }),
    title: `${text}`,
    description: `点击开启${text}`,
    duration: 0,
    closable: true,
    meta: moment().format('h:mm'),
    onClick: () => {
      hide()
      setTimeout(() => isDark.value = !isDark.value, 150)
    }
  })
}, 30000)