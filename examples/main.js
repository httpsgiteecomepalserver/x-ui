import { ViteSSG } from 'vite-ssg'
import xui from 'x-ui-vue3'
import routes from './routes/routes'

// import 'x-ui-vue3/style/index.scss'
// import 'x-ui-vue3/style/dark.scss'

import './styles/theme/light.css'
import './styles/theme/dark.css'

import './styles/demo.scss'
import 'uno.css'

import App from './App.vue'
import ComponentDemo from './utils/ComponentDemo'
import ComponentDemos from './utils/ComponentDemos'
import Doc from './utils/Doc'

export const createApp = ViteSSG(App, { routes, base: '/x-ui/' }, ({ app, router }) => {
  app.use(router)
  app.use(xui)

  app.component(ComponentDemo.name, ComponentDemo)
  app.component(ComponentDemos.name, ComponentDemos)
  app.component(Doc.__name, Doc)
})
