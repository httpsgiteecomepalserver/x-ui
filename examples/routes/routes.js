const demos = import.meta.glob('../../components/**/demos/zhCN/index.demo-entry.md')
const getName = e => e.match(/components\/(.*?)\/demo/)[1]

const componentsRoutes = Object.keys(demos).map(e => ({
  path: getName(e),
  name: getName(e),
  component: demos[e]
}))

export const startRoutes = [
  {
    path: 'getting-started',
    name: 'getting-started',
    component: () => import('/README.md')
  }
]

const routes = [
  {
    path: '/',
    redirect: 'zhComponents'
  },
  {
    name: 'zhComponents',
    path: '/zh-CN/components',
    component: () => import('../pages/Layout.vue'),
    redirect: '/zh-CN/components/form',
    children: [
      ...startRoutes,
      //
      ...componentsRoutes
    ]
  },
  {
    name: '404',
    path: '/:pathMatch(.*)*',
    redirect: {
      name: 'zhComponents'
    }
  }
]

export default routes
