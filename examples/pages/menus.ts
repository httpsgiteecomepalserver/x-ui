import { h } from 'vue'
import { RouterLink } from 'vue-router'

function rednerLabel(label: string, path?: string) {
  path = `./${path || label}`
  return h(RouterLink, { to: path }, () => label)
}

// prettier-ignore
export const menus = [
  {
    type: 'group',
    label: '快速上手',
    children: [
      {
        label: rednerLabel('使用', 'getting-started'),
        value: 'getting-started'
      },
    ]
  },
  {
    type: 'group',
    label: '数据输入',
    children: [
      ...[
        'form', 'date-picker',
        'input', 'switch', 'rate', 'slider',
        'radio', 'checkbox', 'select',
      ].map(e => ({ label: rednerLabel(e), value: e }))
    ]
  },
  {
    type: 'group',
    label: '通用组件',
    children: [
      ...[
        'icon', 'button', 'space', 'card', 'table', 'virtual-list',
        'dropdown', 'tooltip', 'popover',
        'message', 'notification', 'modal', 'drawer',
        'progress', 'tag', 'code', 'image', 'tabs', 'window', 'drag-resize',
        'calendar', 'render'
      ].map(e => ({ label: rednerLabel(e), value: e }))
    ]
  },
  {
    type: 'group',
    label: '导航',
    children: [
      ...[
        'menu', 'anchor'
      ].map(e => ({ label: rednerLabel(e), value: e }))
    ]
  },
  {
    type: 'group',
    label: '过渡组件',
    children: [
      ...[
        'expand-transition'
      ].map(e => ({ label: rednerLabel(e), value: e }))
    ]
  },
  {
    type: 'group',
    label: '指令',
    children: [
      ...[
        'border-light', 'sunken', 'skeleton', 'acrylic',
      ].map(e => ({ label: rednerLabel(e), value: e }))
    ]
  }
]
