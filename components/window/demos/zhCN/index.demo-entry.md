# 窗口 Window

```demo
basic
notepad
type
more
```

## Props

| 名称                | 类型                   | 默认值    | 说明             |
| ------------------- | ---------------------- | --------- | ---------------- |
| type                | `'default'` `'simple'` | `default` | 风格类型         |
| title               | `string \| slot`       | `'small'` | 标题             |
| prefix              | `string \| slot`       | `-`       | 显示在标题之前   |
| suffix              | `string \| slot`       | `-`       | 显示在标题之后   |
| header-style        | `StyleValue`           | `-`       | 顶部样式         |
| body-style          | `StyleValue`           | `-`       | 内容样式         |
| fullscreen(v-model) | `boolean`              | `-`       | 是否全屏         |
| closable            | `boolean`              | `false`   | 是否显示关闭按钮 |

## Event

| 名称  | 回调参数 | 说明               |
| ----- | -------- | ------------------ |
| close | `()`     | 点击关闭按钮的回调 |
