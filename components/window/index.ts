import { App } from 'vue'
import Window from './Window'

Window.install = (app: App) => {
  app.component(Window.name, Window)
}

export default Window