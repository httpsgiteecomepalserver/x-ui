import type { PropType, VNodeChild } from 'vue'
import placements from './placements'
import type { AlignType } from '../align/interface';
import { ActionType } from '../trigger/Trigger';


export default {
  prefixCls: { default: 'x-tooltip' },
  trigger: { type: String as PropType<ActionType>, default: 'hover' },
  disabled: Boolean,
  // popup
  title: [String, Number, Object, Array] as PropType<VNodeChild | JSX.Element>,
  visible: Boolean,
  popupClass: [String, Object],
  popupStyle: [String, Object],
  transitionName: { default: 'zoom-ani' },
  getPopupContainer: Function as PropType<(triggerNode: HTMLElement) => HTMLElement>,
  // popup align
  align: Object as PropType<AlignType>,
  placement: { type: String as PropType<keyof typeof placements>, default: 'top' },
  builtinPlacements: Object as PropType<typeof placements>,
  // target
  mouseEnterDelay: { default: 100 },
  mouseLeaveDelay: { default: 100 },
}