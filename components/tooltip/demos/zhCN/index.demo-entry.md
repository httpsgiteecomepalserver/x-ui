# 文字提示 Tooltip

```demo
basic
trigger
placement
popup-style
```

## Props

| 名称  | 类型                 | 默认值 | 说明     |
| ----- | -------------------- | ------ | -------- |
| title | `VNodeChild \| slot` | `-`    | 提示文字 |

## 共同的 Props

> 以下为 Tooltip、Popover 共享的 Props。

| 名称              | 类型                                                                                                                                                             | 默认值    | 说明                         |
| ----------------- | ---------------------------------------------------------------------------------------------------------------------------------------------------------------- | --------- | ---------------------------- |
| visible(v-model)  | `boolean`                                                                                                                                                        | `false`   | 是否可见                     |
| disabled          | `boolean`                                                                                                                                                        | `false`   | 是否禁用                     |
| placements        | `'left' \| 'right' \| 'top' \| 'bottom' \| 'topLeft' \| 'leftTop' \| 'topRight' \| 'rightTop' \| 'bottomRight' \| 'rightBottom' \| 'bottomLeft' \| 'leftBottom'` | `'top'`   | 弹出位置                     |
| trigger           | `'click' \| 'contextmenu' \| 'focus' \| 'hover'`                                                                                                                 | `'hover'` | 触发方式                     |
| mouse-enter-delay | `number`                                                                                                                                                         | `100`     | 鼠标移入后延时显示, 单位：ms |
| mouse-leave-delay | `number`                                                                                                                                                         | `100`     | 鼠标移出后延时隐藏, 单位：ms |
| popup-class       | `string`                                                                                                                                                         | `-`       | 弹出层类名                   |
| popup-style       | `object`                                                                                                                                                         | `-`       | 弹出层样式                   |

## Event

| 名称           | 回调参数             | 说明 |
| -------------- | -------------------- | ---- |
| visible-change | `(visible: boolean)` | `-`  |
