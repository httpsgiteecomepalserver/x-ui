# 基础用法

```html
<x-space>
  <x-icon type="code" :size="36" />
  <x-icon type="loading" :size="36" spin />
  <x-icon type="check-circle" :size="36" color="#0078d7" />
  <x-icon type="calendar" :size="36" />
</x-space>
```