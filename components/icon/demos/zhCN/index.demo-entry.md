
# 图标 Icon

```demo
basic
```

## Props

| 名称 | 类型 | 默认值 | 说明 |
| --- | --- | --- | --- |
| type | `string` |  | [iconfont](https://www.iconfont.cn/manage/index?spm=a313x.7781069.1998910419.13&manage_type=myprojects&projectId=2698224) |
| spin | `boolean` | `false` | 是否有旋转动画 |
| color | `string` |  | 是否有旋转动画 |
| size | `string` |  | 是否有旋转动画 |