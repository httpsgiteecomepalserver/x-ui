import { App, defineComponent, ExtractPropTypes } from 'vue'
import { formatLength } from '../_util/css'
import IconFont from './IconFont'

const MyIconFont = IconFont({
  scriptUrl: '//at.alicdn.com/t/c/font_2698224_awmh8fka824.js'
})

export const iconProps = {
  type: String,
  spin: Boolean,
  color: String,
  size: [String, Number]
}

export type IconProps = ExtractPropTypes<typeof iconProps>

const Icon = defineComponent({
  name: 'x-icon',
  props: iconProps,
  emits: ['click'],
  setup(props, { emit }) {
    
    function onClick(e) {
      emit('click', e)
    }

    return () => {
      const { spin } = props, prefixCls = 'x-icon'
      const cls = [prefixCls, {
        [`${prefixCls}-spin`]: spin
      }]
      const style = {
        fontSize: formatLength(props.size),
        color: props.color
      }
      
      return <MyIconFont class={cls} style={style} type={props.type} onClick={onClick} />
    }
  }
})

Icon.install = (app: App) => {
  app.component(Icon.name, Icon)
}

export default Icon