# 虚拟滚动

> 请确保 item 高度一致，否则可能会出现抖动，建议给 item 设置固定高度。

```demo
basic
buffer
```

### Props

| 名称        | 类型                           | 默认值  | 说明                       |
| ----------- | ------------------------------ | ------- | -------------------------- |
| items       | `Array`                        | `[]`    | 数据                       |
| item-key    | `string \| ((item) => string)` | `-`     | item 唯一 key              |
| virtual     | `boolean`                      | `false` | 是否开启虚拟滚动           |
| buffer-size | `number`                       | `0`     | 可视区域前后的缓冲元素个数 |

### Event

| 名称   | 回调参数     | 说明 |
| ------ | ------------ | ---- |
| scroll | `(e: Event)` | `-`  |
