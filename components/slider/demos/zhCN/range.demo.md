# 范围

```html
<x-space vertical style="max-width: 300px;">
  阈值 {{ value[0] }} ~ {{ value[1] }}
  <x-slider v-model:value="value" range />
</x-space>
```

```js
export default {
  data() {
    return {
      value: [30, 60]
    }
  }
}
```