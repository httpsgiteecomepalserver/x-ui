# 标记

```html
<x-space vertical>
  你的网速高达 {{ value }}KB/秒，超越了 {{ value }}% 的地球人
  <x-slider v-model:value="value" :marks="marks" />
</x-space>
```

```js
export default {
  data() {
    return {
      value: 50,
      marks: {
        10: '龟速🐌',
        30: '滑板车🛴',
        50: '汽车🚗',
        70: '飞机🛫',
        90: '火箭🚀',
      }
    }
  }
}
```