# 滑动选择 Slider

```demo
basic
range
marks
formatTooltip
disabled
```

### Props

| 名称 | 类型 | 默认值 | 说明 |
| --- | --- | --- | --- |
| marks | `object` | `-` |  Slider 上的标记 |
| disabled | `boolean` | `false` |  是否禁用 |
| step | `number \| null` | `1` | 步长。设置 `step` 为 `null` 时，可选值仅有 marks 标出的部分。 |
| range | `boolean` | `false` | 双滑块模式 |
| min | `number` | `0` |  最小值 |
| max | `number` | `100` |  最大值 |
| tooltip | `boolean` | `true` |  是否展示 tooltip |
| formatTooltip | `	(value: number) => string | number` | `-` | 格式化 tooltip |
| value(v-model) | `number \| number[]` | `-` | 当前值 |

### Event

| 名称 | 回调参数 | 说明 |
| --- | --- | --- |
| change | `(value: number)` |  选择时的回调 |
| hoverChange | `(value: number)` | 鼠标经过时数值变化的回调 |