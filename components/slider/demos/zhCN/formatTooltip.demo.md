# 格式化弹出提示

```html
<x-slider :formatTooltip="formatTooltip"/>
```

```js
export default {
    setup() {
        return {
            formatTooltip(val) {
                return val + '%'
            }
        }
    }
}
```