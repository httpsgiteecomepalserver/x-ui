# 基础用法

```html
<x-space vertical style="max-width: 300px;">
  更改内置显示器的亮度
  <x-slider v-model:value="value" />
</x-space>
```

```js
export default {
    data() {
        return {
            value: 30
        }
    }
}
```