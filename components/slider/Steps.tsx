import { defineComponent } from "vue";
import { isValidElement } from '../_util/props-util';

export default (_, {  attrs }) => {
  const { prefixCls, marks = [] } = attrs

  const dotsNode = Object.keys(marks).map(num => {
    const dotProps = {
      class: `${prefixCls}_dot`,
      style: {
        left: `${num}%`,
      },
      key: num
    }
    return <span {...dotProps}></span>
  })
  
  return <div class={`${prefixCls}_steps`}>{dotsNode}</div>
}