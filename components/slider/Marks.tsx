import { isValidElement } from '../_util/props-util';

export default (_, {  attrs }) => {
  const { prefixCls, marks = {}, min, max } = attrs

  function onClickLabel(e: MouseEvent, num: number) {
    e.stopPropagation()
    e.preventDefault()
    attrs.onClickLabel?.(e, num)
  }

  const marksNode = Object.keys(marks).map(num => {
    const mark = marks[num]
    const isObject = typeof mark === 'object' && !isValidElement(mark)
    const markLabel = isObject ? `${mark.label}` : `${mark}`
    const markProps = {
      class: `${prefixCls}_mark`,
      style: {
        trasnform: 'translateX(-50%)',
        left: `${(+num - min) / (max - min) * 100}%`,
        ...mark?.style
      },
      key: num,
      onMousedown: e => onClickLabel(e, +num),
      onTouchstart: e => onClickLabel(e, +num)
    }
    return <span {...markProps}>{markLabel}</span>
  })
  
  return <div class={`${prefixCls}_marks`}>{marksNode}</div>
}