# 选择器 Select

```demo
basic
multiple
clearable
filter
virtual
custom-opt
disabled
```

### Props

| 名称 | 类型 | 默认值 | 说明 |
| --- | --- | --- | --- |
| value | `string \| string[]` | `-` |  选中值 |
| options | `Array` | `-` |  options 数据 |
| clearable | `boolean` | `false` |  是否可清空 |
| disabled | `boolean` | `false` | 是否禁用 |
| multiple | `boolean` | `false` |  是否多选 |
| show-arrow | `boolean` | `true` |  是否显示箭头 |
| virtual-scroll | `boolean` | `false` | 是否启用虚拟滚动 |
| filterable | `boolean` | `false` |  是否可过滤 |
| filter | `(pattern: string, option: object) => boolean` | `-` | 过滤器函数 |
| placeholder | `boolean` | `false` |  是否可过滤 |
| label | `slot` | `-` |  自定义选项渲染 |


### Event

| 名称 | 回调参数 | 说明 |
| --- | --- | --- |
| change | `(value: string \| string[])` | 值变化时的回调函数 |
| select | `(value: string, option)` | 选中时调用 |
| deselect | `(value: string, option)` | 取消选中时调用 |
| search | `(value: string)` | 	文本框值变化时回调 |