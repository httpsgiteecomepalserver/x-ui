# 显示在指定区域

记得给目标设置

`position: relative;`

`overflow: hidden;`

```html
<x-space vertical>

  <x-card
    id="target"
    style="position: relative; overflow: hidden;"
    content-style="height: 400px; display: flex; flex-direction: column; align-items: center; justify-content: center;">
    <x-button type="primary" style="font-size: 3em;" @click="visible = true">下次一定</x-button>
  </x-card>
  
  <x-drawer v-model:visible="visible" to="#target" width="250">
    <template #title>下次一定~</template>
    <x-space vertical size="16">
      请问您今天要来点兔子吗？？
      <div style="height: 200px; background: url(https://gitee.com/httpsgiteecomepalserver/x-ui/raw/master/images//tibi.gif) center/cover no-repeat;" />
      恋爱小行星
      <div style="height: 170px; background: url(https://gitee.com/httpsgiteecomepalserver/x-ui/raw/master/images/091c239253b95c1c8d32d76ae18bbd14887b8cc4.png) center/cover no-repeat;" />
    </x-space>
  </x-drawer>

</x-space>
```

```js
export default {
  data: () => ({
    visible: false
  })
}
```