# 基础用法

```html
<x-space>
  <x-drawer v-model:visible="visible" title="TITLE">
    <div>长大后</div>
    <div>生活是一块小小的屏幕</div>
    <div>我在外头</div>
    <div>女朋友在里头</div>
  </x-drawer>
  <x-button type="primary" @click="visible = true">OPEN</x-button>
</x-space>
```

```js
export default {
  data: () => ({
    visible: false
  })
}
```