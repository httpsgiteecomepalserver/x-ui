# 嵌套

```html
<x-space>
  <x-drawer v-model:visible="visible" title="TITLE" id="nesting">
    <div style="margin-top: 60px">1111</div>
    <div style="margin-top: 60px">1111</div>
    <div style="margin-top: 60px">1111</div>
    <div style="margin-top: 60px">1111</div>
    <div style="margin-top: 60px">1111</div>
    <div style="margin-top: 60px">1111</div>
    <div style="margin-top: 60px">1111</div>
    <div style="margin-top: 60px">1111</div>
    <div style="margin-top: 60px">1111</div>
    <div style="margin-top: 60px">1111</div>
    <div style="margin-top: 60px">1111</div>
    <div style="margin-top: 60px">1111</div>
    <div style="margin-top: 60px">1111</div>
    <div style="margin-top: 60px">1111</div>
    <div style="margin-top: 60px">1111</div>
    <div style="margin-top: 60px">1111</div>
    <div style="margin-top: 60px">1111</div>
    <div style="margin-top: 60px">1111</div>
    <div style="margin-top: 60px">1111</div>
    <div style="margin-top: 60px">1111</div>
    <div style="margin-top: 60px">1111</div>
  </x-drawer>
  <x-drawer v-model:visible="visible" width="100">
      <div style="margin-top: 60px">1111</div>
      <div style="margin-top: 60px">1111</div>
      <div style="margin-top: 60px">1111</div>
      <div style="margin-top: 60px">1111</div>
      <div style="margin-top: 60px">1111</div>
      <div style="margin-top: 60px">1111</div>
      <div style="margin-top: 60px">1111</div>
      <div style="margin-top: 60px">1111</div>
      <div style="margin-top: 60px">1111</div>
      <div style="margin-top: 60px">1111</div>
      <div style="margin-top: 60px">1111</div>
      <div style="margin-top: 60px">1111</div>
      <div style="margin-top: 60px">1111</div>
      <div style="margin-top: 60px">1111</div>
      <div style="margin-top: 60px">1111</div>
      <div style="margin-top: 60px">1111</div>
      <div style="margin-top: 60px">1111</div>
      <div style="margin-top: 60px">1111</div>
      <div style="margin-top: 60px">1111</div>
      <div style="margin-top: 60px">1111</div>
    </x-drawer>
  <x-button type="primary" @click="visible = true">OPEN</x-button>
</x-space>
```

```js
export default {
  data: () => ({
    visible: false,
  })
}
```