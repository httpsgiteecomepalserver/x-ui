
# 抽屉 Drawer

```demo
basic
header
footer
other
to
```

## Props

| 名称 | 类型 | 默认值 | 说明 |
| --- | --- | --- | --- |
| title | `string \| slot` | `-` | 标题 |
| header | `slot` | `-` | 头部 |
| footer | `slot` | `-` | 页脚 |
| visible(v-model) | `boolean` | `false` | 是否可见 |
| width | `number \| string` | `396` | 宽度 |
| mask | `boolean` | `true` | 是否显示遮罩 |
| mask-closable | `boolean` | `true` | 点击遮住是否关闭 |
| to | `string \| HTMLElement` | `'body'` | 挂载的 HTML 节点 |
