# ~高能玩法！ ψ(｀∇´)ψ

```html
<x-space>
  <x-drawer v-model:visible="visible">
    
    <template #title>
      <x-input placeholder="查找" />
      <div style="margin-top: 1em">列表</div>
    </template>

    <x-space vertical size="15">

      <x-card v-sunken title="提比">
        <template #cover>
          <div style="height: 200px; background: url(https://gitee.com/httpsgiteecomepalserver/x-ui/raw/master/images//tibi.gif) center/cover no-repeat;" />
        </template>
      </x-card>
      
      <x-card v-sunken title="请问您今天要来点兔子吗？？">
        <template #cover>
          <div style="height: 350px; background: url(https://gitee.com/httpsgiteecomepalserver/x-ui/raw/master/images//b8cf93d74af443f8dfcd2adc808a1a2545264254.jpg) center/cover no-repeat;" />
        </template>
        <div style="overflow: hidden; text-overflow: ellipsis; display: -webkit-box; -webkit-line-clamp: 4; -webkit-box-orient: vertical;">
          简介：作品的舞台是一家咖啡店，在这家店中，以天真烂漫的少女·心爱为中心，讲述了角色们愉快的日常生活。在作品当中登场的妹子们包括酷酷的智乃、有着军人气质的理世、充满和风的千夜、看起来是大小姐实际却非常贫穷的纱路，可以说每个角色都充满魅力。而它不光有着“治愈系”的招牌，同时也是在可爱的世界中表现出“搞笑”的作品。...
        </div>
      </x-card>

      <x-card v-sunken title="输了游戏">赢了人生</x-card>

      <x-card v-sunken title="恋爱小行星">
        <template #cover>
          <div style="height: 350px; background: url(https://gitee.com/httpsgiteecomepalserver/x-ui/raw/master/images/091c239253b95c1c8d32d76ae18bbd14887b8cc4.png) center/cover no-repeat;" />
        </template>
        <div style="overflow: hidden; text-overflow: ellipsis; display: -webkit-box; -webkit-line-clamp: 4; -webkit-box-orient: vertical;">
          简介：木之幡米拉在小时候与在露营地遇见的男生约定，“要找到小行星”。上了高中后她本打算加入天文社，但从今年开始“天文社”就和“地质研究会”合并成了“地学社”……！？和地学系女孩们一起，去寻找各种各样闪闪发亮的东西吧。...
        </div>
      </x-card>

      <x-card v-sunken title="体育老师是怎样练成的"></x-card>
      <x-card v-sunken title="永远不可能发生在你身上的事"></x-card>
      <x-card v-sunken title="O哦呼-~"></x-card>
    </x-space>

    <template #footer>
      <x-space align="center" justify="center" size="14" style="width: 90%; margin: 10px auto">
        <span style="font-size: 1.8em">☼</span>
        <x-slider :value="75" style="width: 250px" />
      </x-space>
      <x-space item-style="flex: 1">
        <x-button type="primary" block @click="visible = false">确认</x-button>
        <x-button block @click="visible = false">取消</x-button>
      </x-space>
    </template>

  </x-drawer>
  <x-button type="primary" @click="visible = true">OPEN</x-button>
</x-space>
```

```js
export default {
  data: () => ({
    visible: false
  })
}
```