# 自定义 Footer

使用插槽 `#footer`

```html
<x-space>
  <x-drawer v-model:visible="visible">
    
    <template #header>
      <x-space>
        <x-rate :value="2" character="☼" color="#f0a020" />
        <span>(o゜▽゜)o☆</span>
      </x-space>
    </template>

    <div>树叶有砖工</div>

    <template #footer>
      <x-space item-style="flex: 1">
        <x-button type="primary" block @click="visible = false">确认</x-button>
        <x-button block @click="visible = false">取消</x-button>
      </x-space>
    </template>

  </x-drawer>
  <x-button @click="visible = true">OPEN</x-button>
</x-space>
```

```js
export default {
  data: () => ({
    visible: false
  })
}
```