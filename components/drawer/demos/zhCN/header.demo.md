# 自定义 Header

使用插槽 `#header`

```html
<x-space>
  <x-drawer v-model:visible="visible">
    <template #header>
      <x-space>
        <x-rate :value="2" character="☼" color="#f0a020" />
        <span>(o゜▽゜)o☆</span>
      </x-space>
    </template>

    <div>树叶有砖工</div>

  </x-drawer>
  <x-button @click="visible = true">OPEN</x-button>
</x-space>
```

```js
export default {
  data: () => ({
    visible: false
  })
}
```