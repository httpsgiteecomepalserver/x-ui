import { App } from 'vue'
import Render from './Render'

// @ts-ignore
Render.install = (app: App) => {
  app.component('x-render', Render)
}

export default Render
export { Render }
