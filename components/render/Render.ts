import { resolveUnref } from '@vueuse/core'
import { VNodeChild } from 'vue'

interface Props<T> {
  render: VNodeChild | ((args?: T) => VNodeChild)
  data?: T
}

export default function Render<T>({ render, data }: Props<T>) {
  return typeof render == 'function' ? render(data) : resolveUnref(render)
}
