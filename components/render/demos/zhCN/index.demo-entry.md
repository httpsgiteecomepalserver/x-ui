# 渲染 Render

```demo
basic
curry
```

## Props

| 名称   | 类型                                 | 默认值 | 说明 |
| ------ | ------------------------------------ | ------ | ---- |
| render | `VNodeChild \| (data) => VNodeChild` | `-`    |      |
| data   | `any`                                | `-`    |      |
