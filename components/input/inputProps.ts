import { PropType } from 'vue'
import { Arrayable } from '@vueuse/core'

export type Autosize = boolean | Partial<{ minRows: number; maxRows: number }>

export default {
  value: [String, Array] as PropType<Arrayable<string>>,
  placeholder: [String, Array] as PropType<Arrayable<string>>,
  type: String,
  disabled: Boolean,
  prefix: String,
  suffix: String,
  addonBefore: String,
  addonAfter: String,
  pair: Boolean,
  separator: { type: String, default: '-' },
  autofocus: Boolean,
  clearable: Boolean,
  autosize: [Boolean, Object] as PropType<Autosize>
}
