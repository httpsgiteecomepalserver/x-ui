# 可清除

设置 `clearable` 可清空

```html
<x-space vertical>
  <x-input clearable placeholder="和我定下契约 成为魔法少女吧！" />
  textarea
  <x-input type="textarea" clearable autosize placeholder="隐藏着黑暗力量的钥匙啊，在我面前显示你真正的力量！ 现在以你的主人…………" />
</x-space>
```
