# 前置/后置标签

`addon` 演示

```html
<x-space vertical>
  前置
  <x-input addon-before="http://" />

  前置 & 后置
  <x-input addon-before="💲" addon-after="美元" />

  插槽 #addonBefore
  <x-input>
    <template #addonBefore>
      <x-icon type="calendar" />
    </template>
  </x-input>
</x-space>
```
