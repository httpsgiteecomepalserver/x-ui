# 输入成对值

```html
<x-input v-model:value="value" :placeholder="['from', 'to']" pair />
```

```js
import { ref } from 'vue'

export default {
  setup() {
    return {
      value: ref([])
    }
  }
}
```
