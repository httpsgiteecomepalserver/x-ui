# 前缀 & 后缀

```html
<x-space vertical>
  前缀
  <x-input prefix="™" />

  前缀 & 后缀
  <x-input prefix="💲" suffix="美元" />

  插槽 #prefix
  <x-input>
    <template #prefix>
      <x-icon type="calendar" />
    </template>
  </x-input>
</x-space>
```