# 基础用法

```html
<x-input v-model:value="value" placeholder="哦呼！" />
```

```js
import { ref } from 'vue'

export default {
  setup() {
    return {
      value: ref('')
    }
  }
}
```
