# 搜索框

搜索框 `x-input-search` 演示

```html
<x-space vertical>
  <x-input-search />

  enterButton
  <x-input-search enterButton placeholder="轻松开学季，好服务全场6折起" />

  插槽 #enterButton
  <x-input-search>
    <template #enterButton>
      <x-button type="primary"><x-icon type='check'/></x-button>
    </template>
  </x-input-search>
</x-space>
```