# 文本输入 Input

```demo
basic
icon
addon
clearable
pair
textarea
disabled
```

## Props

| 名称           | 类型             | 默认值  | 说明         |
| -------------- | ---------------- | ------- | ------------ |
| value(v-model) | `string`         | `-`     | 文本输入的值 |
| prefix         | `string \| slot` | `-`     | 前缀         |
| suffix         | `string \| slot` | `-`     | 后缀         |
| addon-before   | `string \| slot` | `-`     | 前置标签     |
| addon-after    | `string \| slot` | `-`     | 后置标签     |
| disabled       | `boolean`        | `false` | 是否禁用     |
| clearable      | `boolean`        | `false` | 是否可清空   |

## Event

| 名称       | 回调参数             | 说明                             |
| ---------- | -------------------- | -------------------------------- |
| change     | `(value: string)`    | 输入框内容变化且失去焦点时的回调 |
| pressEnter | `(e: KeyboardEvent)` | 按下回车的回调                   |
