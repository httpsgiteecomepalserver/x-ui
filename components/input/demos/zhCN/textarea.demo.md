# 文本域

```html
<x-space vertical>
  自动大小
  <x-input type="textarea" value="(^>-<^)" autosize />
  最小2行
  <x-input type="textarea" value="(^_^)" :autosize="{ minRows: 2 }" />
  最大5行
  <x-input type="textarea" value="0>_<0" :autosize="{ minRows: 2, maxRows: 5 }" />
</x-space>
```
