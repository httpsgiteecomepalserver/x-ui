import type { App } from 'vue'
import Input from './input'

import InputSearch from './search'

export { default as Input } from './input'
export { default as InputSearch } from './search'

Input.install = (app: App) => {
  app.component(Input.name, Input)
  app.component(InputSearch.name, InputSearch)
}

export default Input
