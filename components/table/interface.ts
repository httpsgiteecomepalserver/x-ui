import { InjectionKey, Ref, VNodeChild, VNodeTypes } from "vue";
import { TableProps } from ".";

export type DataTableContext = {
  props: TableProps
  leftFixedColumsRef: Ref<TableColumn[]>
  rightFixedColumsRef: Ref<TableColumn[]>
  fixedColumnLeftMapRef: Ref<Record<ColumnKey, number>>
  fixedColumnRightMapRef: Ref<Record<ColumnKey, number>>
}

export const dataTableContextKey: InjectionKey<DataTableContext> = Symbol('dataTable')

export type ColumnKey = string | number

export type RowData = {
  [key: string]: unknown
}

export type TableColumn<T = RowData> = {
  title: VNodeChild | ((column: TableColumn) => VNodeChild)
  key: ColumnKey

  render?: (rowData: T, rowIndex: number) => VNodeChild

  fixed?: 'left' | 'right'
  width?: number
  align?: 'left' | 'center' | 'right'
  className?: string | ((column: TableColumn, index: number) => any)
}