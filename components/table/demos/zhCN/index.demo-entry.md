# 表格 Table

<!--single-column-->

```demo
basic
bordered
fixed-header
fixed-column
virtual
```

### DataTable Props

| 名称            | 类型                                           | 默认值     | 说明               |
| --------------- | ---------------------------------------------- | ---------- | ------------------ |
| bordered        | `boolean`                                      | `false`    | 是否显示边框       |
| bottom-bordered | `boolean`                                      | `true`     | 是否显示低边框     |
| columns         | `Array<TableColumn>`                           | `[]`       | 表格列             |
| data            | `Array<object>`                                | `[]`       | 表格数据           |
| row-props       | `(rowData, rowIndex : number) => object`       | `-`        | 自定义行属性       |
| row-class       | `any \| ((rowData, rowIndex : number) => any)` | `-`        | 自定义行类名       |
| scroll-x        | `number`                                       | `-`        | 设置横向滚动的宽度 |
| virtual-scroll  | `boolean`                                      | `false`    | 是否开启虚拟滚动   |
| size            | `'small'` `'medium'` `'large'`                 | `'medium'` | 表格的尺寸         |

### TableColumn

| 名称       | 类型                                                   | 默认值   |
| ---------- | ------------------------------------------------------ | -------- |
| title      | `VNodeChild \| ((column: TableColumn) => VNodeChild)`  | `-`      |
| key        | `string \| number`                                     | `-`      |
| fixed?     | `'left'` `'right'`                                     | `-`      |
| width?     | `number`                                               | `-`      |
| align      | `'left'` `'right'` `'center'`                          | `'left'` |
| className? | `any \| ((column: TableColumn, index: number) => any)` | `-`      |
