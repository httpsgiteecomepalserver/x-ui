import { TableProps } from ".";
import { ColumnKey, RowData, TableColumn } from "./interface";

export function getRowClassName(rowCLassName: TableProps['rowClass'], row: RowData, index: number) {
  if (typeof rowCLassName === 'function') {
    return rowCLassName(row, index)
  } else {
    return rowCLassName
  }
}

export function getColumnClassName(column: TableColumn, index: number) {
  const { className } = column
  if (typeof className === 'function') {
    return className(column, index)
  } else {
    return className
  }
}

export function renderTitle(column: TableColumn) {
  return typeof column.title === 'function'
    ? column.title(column)
    : column.title
}