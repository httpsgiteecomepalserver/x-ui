# 插槽

```html
<x-card title="卡片" style="width: 300px">
  <div>卡片内容</div>
  <template #header-extra>
    <x-icon type="check-circle" :size="24" color="#0078d7" />
  </template>
  <template #footer>
    #footer
  </template>
</x-card>
```