
# 卡片 Card

```demo
basic
cover
bordered
hoverable
slots
```

## Props

| 名称 | 类型 | 默认值 | 说明 |
| --- | --- | --- | --- |
| title | `string \| slot` | `-` | 标题 |
| bordered | `boolean` | `true` | 	是否有边框 |
| hoverable | `boolean` | `false` | 是否可悬浮 |
| size | `'small' \| 'medium' \| 'large'` | `'medium'` | 卡片尺寸 |
| contentStyle | `object \| string` | `-` | 内容区域的样式 |
| footerStyle | `object \| string` | `-` | 底部区域的样式 |
| headerStyle | `object \| string` | `-` | 头部区域的样式 |
| cover | `slot` | `-` | 卡片封面 |
| header | `slot` | `-` | 卡片头部 |
| header-extra | `slot` | `-` | 卡片右上角的操作区域 |
| footer | `slot` | `-` | 底部内容 |