# 封面

```html
<x-space style="overflow: auto;" :size='12' :wrap='false'>

  <x-card title="恋爱小行星" style="width: 225px">
    <template #cover>
      <div style="height: 300px; background: url(https://gitee.com/httpsgiteecomepalserver/x-ui/raw/master/images/091c239253b95c1c8d32d76ae18bbd14887b8cc4.png) center/cover no-repeat;" />
    </template>
    <div style="overflow: hidden; text-overflow: ellipsis; display: -webkit-box; -webkit-line-clamp: 4; -webkit-box-orient: vertical;">
      简介：木之幡米拉在小时候与在露营地遇见的男生约定，“要找到小行星”。上了高中后她本打算加入天文社，但从今年开始“天文社”就和“地质研究会”合并成了“地学社”……！？和地学系女孩们一起，去寻找各种各样闪闪发亮的东西吧。...
    </div>
  </x-card>

  <x-card title="开挂药师的奇幻世界悠闲生活" style="width: 225px">
    <template #cover>
      <div style="height: 300px; background: url(https://gitee.com/httpsgiteecomepalserver/x-ui/raw/master/images/c17c6b34abce09d2ad7441d69619397410c12fe0.jpg) center/cover no-repeat;" />
    </template>
     <div style="overflow: hidden; text-overflow: ellipsis; display: -webkit-box; -webkit-line-clamp: 4; -webkit-box-orient: vertical;">
      简介：作为社畜每日辛勤工作的男人，桐尾礼治。今天也顶着死鱼眼朝公司走去，不知不觉中身处在异世界的森林之中。…啊，是最近流行的异世界转生啊。拥有的技能是「鉴定、制药」这两个，就这…？算了。虽然是这么想的，但是这两个技能似乎是超乎想象的开挂技能！不断地创造出这个世界里不存在的回复药等等，转眼间就大赚一笔。绝对不可能再作为社畜工作了！下定决心要过上悠闲生活，开了一家药店。以异世界的药店为舞台，悠闲生活的故事即将开始。...
    </div>
  </x-card>

  <x-card title="请问您今天要来点兔子吗？？" style="width: 225px">
    <template #cover>
      <div style="height: 300px; background: url(https://gitee.com/httpsgiteecomepalserver/x-ui/raw/master/images//b8cf93d74af443f8dfcd2adc808a1a2545264254.jpg) center/cover no-repeat;" />
    </template>
     <div style="overflow: hidden; text-overflow: ellipsis; display: -webkit-box; -webkit-line-clamp: 4; -webkit-box-orient: vertical;">
      简介：作品的舞台是一家咖啡店，在这家店中，以天真烂漫的少女·心爱为中心，讲述了角色们愉快的日常生活。在作品当中登场的妹子们包括酷酷的智乃、有着军人气质的理世、充满和风的千夜、看起来是大小姐实际却非常贫穷的纱路，可以说每个角色都充满魅力。而它不光有着“治愈系”的招牌，同时也是在可爱的世界中表现出“搞笑”的作品。...
    </div>
  </x-card>
  
  <x-card title="珈百璃的堕落" style="width: 225px">
    <template #cover>
      <div style="height: 300px; background: url(https://gitee.com/httpsgiteecomepalserver/x-ui/raw/master/images//7b9a5a6e6c5ca157a4c18b01093a7bb1701f4606.jpg) center/cover no-repeat;" />
    </template>
     <div style="overflow: hidden; text-overflow: ellipsis; display: -webkit-box; -webkit-line-clamp: 4; -webkit-box-orient: vertical;">
      简介：天使学校的首席天使来到了地球！ ……但是，住惯了地球的她，开始过起整天逃学打网游的自甘堕落生活。化身为怠惰的废柴天使，简称“废天使”的加百列，早就把让全部人类幸福的目标抛到九霄云外，并发誓要充分享受娱乐——。...
    </div>
  </x-card>
  
</x-space>
```