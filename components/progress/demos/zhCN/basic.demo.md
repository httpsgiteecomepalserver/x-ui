# 基础用法

```html
<x-space vertical style="max-width: 400px">
  <x-progress :percent="30" />
  <x-progress :percent="50" status="success" />
  <x-progress :percent="60" status="warning" />
  <x-progress :percent="80" status="error" />
  <x-progress :percent="100" />
</x-space>
```