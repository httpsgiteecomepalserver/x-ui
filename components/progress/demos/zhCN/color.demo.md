# 自定义颜色

```html
<x-space vertical style="max-width: 400px">
  普通颜色
  <x-progress :percent="percent" color="#6044b9" />
  渐变色 linear-gradient
  <x-progress :percent="percent" color="linear-gradient(to right, rgb(16, 142, 233) 0%, rgb(135, 208, 104) 100%)" />
  根据进度改变颜色
  <x-progress :percent="percent" :color="colors" />
  method
  <x-progress :percent="percent" :color="method" />

  <x-space>
    <x-button @click="percent -= percent <= 0 ? 0 : 20">-</x-button>
    <x-button @click="percent += percent >= 100 ? 0 : 20">+</x-button>
  </x-space>

</x-space>
```

```js
export default {
  data: () => ({
    percent: 20,
    colors: {
      20: '#f56c6c',
      40: '#e6a23c',
      60: '#5cb87a',
      80: '#1989fa',
      100: '#6f7ad3'
    },
    method: percent => percent < 50 ? '#e6a23c' : '#1989fa'
  })
}
```