# 内部显示

```html
<x-space vertical style="max-width: 400px">
  <x-progress :percent="30" text-inside />
  <x-progress :percent="50" status="success" text-inside />
  <x-progress :percent="60" status="warning" text-inside />
  <x-progress :percent="80" status="error" text-inside />
  <x-progress :percent="100" text-inside />
</x-space>
```