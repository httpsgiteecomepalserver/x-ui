
# 进度条 Progress

```demo
basic
inside
color
text
win10
```

## Props

| 名称 | 类型 | 默认值 | 说明 |
| --- | --- | --- | --- |
| percent | `number` | `0` | 百分比 |
| color | `string \| object \| ((percent: number) => string)` | `-` | 进度条颜色 |
| stroke-width | `number` | `8` | 进度条宽度 |
| rail-color | `string \| object \| ((percent: number) => string)` | `-` | 轨道的颜色 |
| rail-style | `StyleValue` | `396` | 轨道的样式 |
| status | `'default' \| 'success' \| 'error' \| 'warning'` | `'default'` | 进度条状态 |
| text-inside | `boolean` | `false` | 进度条显示文字内置在进度条内 |
| show-text | `boolean` | `true` | 是否显示进度条文字内容 |
| format | `slot \| ((percent: number) => VNodeChild)` | `-` | 是否显示进度条文字内容 |