# 自定义文字格式

```html
<x-space vertical style="max-width: 400px">
  自定义
  <x-progress :percent="30 + percent" :format="percent => percent + '#'" />
  自定义
  <x-progress :percent="100 - percent" :format="() => '😜'" />
  插槽 #format
  <x-progress :percent="50 + percent" status="success" text-inside>
    <template #format="percent">{{ 'o((>ω< ))o' + ' ____ ' + percent + '%' }}</template>
  </x-progress>

  <x-space>
    <x-button @click="percent -= percent <= 0 ? 0 : 20">-</x-button>
    <x-button @click="percent += percent >= 60 ? 0 : 20">+</x-button>
  </x-space>

</x-space>
```

```js
export default {
  data: () => ({
    percent: 40
  })
}
```