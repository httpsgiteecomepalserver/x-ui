import { App } from 'vue'
import Anchor from './Anchor.vue'
import AnchorLink from './AnchorLink.vue'

export { default as Anchor } from './Anchor.vue'
export { default as AnchorLink } from './AnchorLink.vue'

Anchor.install = (app: App) => {
  app.component(Anchor.name, Anchor)
  app.component(AnchorLink.name, AnchorLink)
}

export default Anchor