# 锚点 Anchor

<!--single-column-->

```demo
basic
type
sticky
container
```

## Anchor Props

| 名称            | 类型                    | 默认值 | 说明                 |
| --------------- | ----------------------- | ------ | -------------------- |
| type            | `'rail' \| 'block'`     | `rail` | 风格                 |
| show-background | `boolean`               | `true` | 是否展示 link 的背景 |
| offset          | `number`                | `0`    | 锚点滚动捕获的偏移量 |
| container       | `string \| HtmlElement` | `-`    | 锚点的容器           |
| scroll-duration | `number`                | `350`  | 滚动持续时间         |

## AnchorLink Props

| 名称  | 类型     | 默认值 | 说明 |
| ----- | -------- | ------ | ---- |
| title | `string` |        |      |
| href  | `string` |        |      |
