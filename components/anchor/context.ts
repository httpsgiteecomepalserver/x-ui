import { InjectionKey, Ref } from 'vue'
import { AnchorProps } from './Anchor.vue'

export interface AnchorContextProps {
  value: Ref<string>
  links: AnchorLinkContextProps[]
  setActive(href: string)
  props: AnchorProps
}

export interface AnchorLinkContextProps {
  el: HTMLElement
  indent: number
  href: string
}

export const AnchorContextKey: InjectionKey<AnchorContextProps> = Symbol('AnchorContextKey')
export const AnchorLiinkContextKey: InjectionKey<AnchorLinkContextProps> = Symbol('AnchorLiinkContextKey')