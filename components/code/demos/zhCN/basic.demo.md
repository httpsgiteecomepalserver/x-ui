# 基础用法

面试官：请写一个排序。😂

```html
<x-code language="js" :code='`
function sleep(t) {
  return new Promise(resolve => setTimeout(resolve, t))
}

async function sleepSort(arr) {
  const array = []
  const promises = arr.map(e => sleep(e).then(() => array.push(e)))
  await Promise.all(promises)
  return array
}
`'/>
```