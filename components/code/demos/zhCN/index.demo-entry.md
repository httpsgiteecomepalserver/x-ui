# 代码 Code

```demo
basic
```

### Props

| 名称 | 类型 | 默认值 | 说明 |
| --- | --- | --- | --- |
| code | `string` | `''` |  |
| language | `string` | `-` |  |
| trim | `boolean` | `true` |  |