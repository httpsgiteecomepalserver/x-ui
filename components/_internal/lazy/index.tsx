import { defineComponent, toRef } from 'vue'
import { useFalseUntilTruthy } from '../../_util/hooks'

export default defineComponent({
  name: 'Lzay',
  props: {
    visible: Boolean
  },
  setup(props, { slots }) {
    const visibleRef = useFalseUntilTruthy(toRef(props, 'visible'))

    return () => {
      if (!visibleRef.value) return
      return slots.default?.()
    }
  }
})