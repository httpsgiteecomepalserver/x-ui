import { defineComponent, PropType, toRef, vShow, withDirectives } from 'vue'
import { useFalseUntilTruthy } from '../../_util/hooks'
import { getSlot } from '../../_util/props-util'

export type DisplayDirectiveType = 'if' | 'show' | 'lazy:show'

const asd = defineComponent({
  name: 'display-directive',
  props: {
    visible: Boolean,
    type: { type: String as PropType<DisplayDirectiveType>, default: 'if' }
  },
  setup(props, { slots }) {
    const untilTruthy = useFalseUntilTruthy(toRef(props, 'visible'))

    return () => {
      const visible = props.visible
      switch (props.type) {
        case 'if':
          return visible ? slots.default?.() : undefined
        case 'show':
          return withDirectives(getSlot(slots)[0], [[vShow, visible]])
        case 'lazy:show':
          return untilTruthy.value ? withDirectives(getSlot(slots)[0], [[vShow, visible]]) : undefined
      }
    }
  }
})

export default asd