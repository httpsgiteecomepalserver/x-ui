# type

```html
<x-space>
  <x-button loading>Default</x-button>
  <x-button type="primary">Primary</x-button>
  <x-button type="success">Success</x-button>
  <x-button type="warning">Warning</x-button>
  <x-button type="danger">Danger</x-button>
</x-space>
```