# 按钮 Button

```demo
basic
dashed
circle
round
ghost
link
block
disabled
```

## Props

| 名称      | 类型                                              | 默认值      | 说明                                         |
| --------- | ------------------------------------------------- | ----------- | -------------------------------------------- |
| autofocus | `boolean`                                         | `false`     | 自动聚焦                                     |
| block     | `boolean`                                         | `false`     | 按钮是否显示为块级                           |
| circle    | `boolean`                                         | `false`     | 按钮是否为圆形                               |
| dashed    | `boolean`                                         | `false`     | 按钮边框是否为虚线                           |
| disabled  | `boolean`                                         | `false`     | 按钮是否禁用                                 |
| ghost     | `boolean`                                         | `false`     | 按钮是否透明                                 |
| round     | `boolean`                                         | `false`     | 按钮是否显示圆角                             |
| type      | `'default' \| 'primary' \| 'warning' \| 'danger'` | `'default'` | 按钮的类型                                   |
| href      | `string`                                          | `-`         | 跳转的地址                                   |
| target    | `string`                                          | `-`         | 相当于 a 链接的 target 属性，href 存在时生效 |
