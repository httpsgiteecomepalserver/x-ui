# block

```html
<x-space vertical>
  <x-button block>Default</x-button>
  <x-button type="primary" block>Primary</x-button>
  <x-button type="success" block>success</x-button>
  <x-button type="warning" block>Warning</x-button>
  <x-button type="danger" block>danger</x-button>
</x-space>
```