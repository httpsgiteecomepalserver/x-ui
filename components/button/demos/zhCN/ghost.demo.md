# ghost

```html
<x-space>
  <x-button ghost>Default</x-button>
  <x-button type="primary" ghost>Primary</x-button>
  <x-button type="success" ghost>success</x-button>
  <x-button type="warning" ghost>Warning</x-button>
  <x-button type="danger" ghost>danger</x-button>
</x-space>
```