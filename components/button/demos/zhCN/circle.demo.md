# circle

```html
<x-space>
  <x-button circle></x-button>
  <x-button type="primary" circle></x-button>
  <x-button type="success" circle></x-button>
  <x-button type="warning" circle></x-button>
  <x-button type="danger" circle></x-button>
</x-space>
```