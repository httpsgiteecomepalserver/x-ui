# link

```html
<x-space>
  <x-button link>Default</x-button>
  <x-button type="primary" link>Primary</x-button>
  <x-button type="success" link>success</x-button>
  <x-button type="warning" link>Warning</x-button>
  <x-button type="danger" link>danger</x-button>
</x-space>
```