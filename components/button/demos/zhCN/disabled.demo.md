# disabled

```html
<x-space>
  <x-button disabled>Default</x-button>
  <x-button type="primary" disabled>Primary</x-button>
  <x-button type="success" disabled>success</x-button>
  <x-button type="warning" disabled>Warning</x-button>
  <x-button type="danger" disabled>danger</x-button>
</x-space>
```