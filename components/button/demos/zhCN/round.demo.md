# round

```html
<x-space>
  <x-button round>Default</x-button>
  <x-button type="primary" round>Primary</x-button>
  <x-button type="success" round>success</x-button>
  <x-button type="warning" round>Warning</x-button>
  <x-button type="danger" round>danger</x-button>
</x-space>
```