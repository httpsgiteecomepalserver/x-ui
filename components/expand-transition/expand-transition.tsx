import useExpandTransition from './useExpandTransition'
import { defineComponent, Transition, PropType, computed, ExtractPropTypes, TransitionProps } from 'vue'

export const expandTransitionProps = {
  appear: Boolean,
  name: { type: String, default: 'expand-transition' },
  height: { type: [Number, Function], default: 0 },
  width: [Number, Function],
  widthProp: { type: String, default: 'maxWidth' },
  heightProp: { type: String, default: 'maxHeight' },
  scheduler: Function as PropType<(fn: (el?: HTMLElement) => void) => void>
}

export type ExpandTransitionProps = ExtractPropTypes<typeof expandTransitionProps>

const ExpandTransition = defineComponent({
  name: 'x-expand-transition',
  props: expandTransitionProps,
  inheritAttrs: false,
  setup(props, { attrs, slots }) {

    const events = useExpandTransition(props)

    const propsRef = computed(() => ({
      name: props.name,
      appear: props.appear,
      ...events,
      onEnter: (el) => {
        if (typeof props.scheduler === 'function') {
          props.scheduler(events.onEnter.bind(null, el))
        } else {
          events.onEnter(el)
        }
      }
    }))
    
    return () => {
      return <Transition {...attrs} {...propsRef.value} v-slots={slots} />
    }
  }
})

export default ExpandTransition