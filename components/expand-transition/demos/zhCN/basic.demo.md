# 基础用法

```html
<x-space>

  <div>
    <x-switch v-model:value="open1" />
    <x-expand-transition :name="name">
      <div v-show="open1">
        <x-tag nowrap size="large">每天一遍,青春无极限!</x-tag>
      </div>
    </x-expand-transition>
  </div>

  <div>
    <x-switch v-model:value="open2" />
    <x-expand-transition :width='0' :height="null">
      <div v-show="open2">
        <x-tag nowrap size="large">每天一遍,青春无极限!</x-tag>
      </div>
    </x-expand-transition>
  </div>

  <div>
    <x-switch v-model:value="open3" />
    <x-expand-transition :width='0'>
      <div v-show="open3">
        <x-tag nowrap size="large">每天一遍,青春无极限!</x-tag>
      </div>
    </x-expand-transition>
  </div>

  <div>
    <x-switch v-model:value="open4" />
    <x-expand-transition :width="w => w * .75" :height="null">
      <div v-show="open4">
        <x-tag nowrap size="large">每天一遍,青春无极限!</x-tag>
      </div>
    </x-expand-transition>
  </div>

  <div>
    <x-switch v-model:value="open5" />
    <x-expand-transition :height="h => h * .5">
      <div v-show="open5">
        <x-tag nowrap size="large">每天一遍,青春无极限!</x-tag>
      </div>
    </x-expand-transition>
  </div>

</x-space>
```

```js
export default {
  data() {
    return {
      open1: true,
      open2: true,
      open3: true,
      open4: true,
      open5: true,
      open6: true,
    }
  }
}
```