# 展开过渡 ExpandTransition

<!--single-column-->

```demo
basic
```

### Props

| 名称 | 类型 | 默认值 | 说明 |
| --- | --- | --- | --- |
| width | `number \| ((scrollWidth: number) => number)` | `-` |  |
| height | `number \| ((scrollHeight: number) => number)` | `0` |  |

>继承了 [TransitionProps](https://vue3js.cn/docs/zh/guide/transitions-enterleave.html#javascript-%E9%92%A9%E5%AD%90)