import { defineComponent, ref } from 'vue'
import ExpandTransition from './expand-transition'

const ExpandTransition2 = defineComponent({
  name: 'x-expand-transition2',
  props: {
    visible: Boolean,
    placement: String,
  },
  setup(props, { slots }) {
    const div = ref<HTMLElement>()

    const transitionProps = {
      widthProp: 'width',
      heightProp: 'height',
      style: 'display: flex; justify-content: flex-end; align-items: flex-end;',
      onEnter(el: HTMLElement) {
        div.value.setAttribute('style', `width: ${div.value.scrollWidth}px; height: ${div.value.scrollHeight}px;`)
      },
      onAfterEnter(el: HTMLElement) {
        div.value.setAttribute('style', '')
      },
      onBeforeLeave(el: HTMLElement) {
        div.value.setAttribute('style', `width: ${div.value.scrollWidth}px; height: ${div.value.offsetHeight}px;`)
      },
      onAfterLeave(el: HTMLElement) {
        div.value.setAttribute('style', '')
      },
    }

    return () => (
      <ExpandTransition {...transitionProps}>
        <div v-show={props.visible} style='overflow: unset'>
          <div ref={div}>{{...slots}}</div>
        </div>
      </ExpandTransition>
    )
  }
})

export default ExpandTransition2