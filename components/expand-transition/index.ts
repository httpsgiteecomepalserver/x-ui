import { App } from 'vue'
import ExpandTransition from './expand-transition'
export { expandTransitionProps } from './expand-transition'

ExpandTransition.install = (app: App) => {
  app.component(ExpandTransition.name, ExpandTransition)
}

export default ExpandTransition
