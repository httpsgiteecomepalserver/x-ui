import { defineComponent } from 'vue'
import Tooltip from '../tooltip'
import tooltipProps from '../tooltip/tooltipProps'
import { getComponent } from '../_util/props-util';

export default defineComponent({
  name: 'x-popover',
  props: {
    ...tooltipProps,
    prefixCls: { default: 'x-popover' },
    content: String
  },
  setup(props, { slots }) {
    const { prefixCls } = props
    
    return self => {
      const title = getComponent(self, 'title')
      const content = getComponent(self, 'content')
      const tooltipProps = {
        ...props,
        title: (
          <div>
            {title && <div class={`${prefixCls}-title`}>{title}</div>}
            <div class={`${prefixCls}-content`}>{content}</div>
          </div>
        )
      }
      return (
        <Tooltip {...tooltipProps}>
          {slots.default?.()}
        </Tooltip>
      )
    }
  }
})