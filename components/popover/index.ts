import type { App } from 'vue'
import Popover from './Popover'

Popover.install = (app: App) => {
  app.component(Popover.name, Popover)
}

export default Popover