# 气泡卡片 Popover

```demo
basic
slot
```

## Props

| 名称    | 类型             | 默认值 | 说明     |
| ------- | ---------------- | ------ | -------- |
| title   | `string \| slot` | `-`    | 卡片标题 |
| content | `string \| slot` | `-`    | 卡片内容 |

> 更多属性请参考 [Tooltip](./tooltip)
