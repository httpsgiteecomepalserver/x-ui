# 触发方式

```html
<x-space>
  <x-tooltip title="~ 洁白 ~" trigger="hover">
    <x-button>悬浮</x-button>
  </x-tooltip>
  <x-tooltip title="~ 闪耀 ~" trigger="click">
    <x-button>点击</x-button>
  </x-tooltip>
  <x-tooltip title="~ 奇迹之花 ~" trigger="contextmenu">
    <x-button>右键</x-button>
  </x-tooltip>
  <x-tooltip title="~ White Lily ~" trigger="focus">
    <x-button>聚焦</x-button>
  </x-tooltip>
</x-space>
```