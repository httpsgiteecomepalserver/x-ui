# 弹出层样式

```html
<x-tooltip title="可爱即正义" :popup-style="{ minWidth: '150px', border: '1px solid #0078d7' }">
  <x-button>呐</x-button>
</x-tooltip>
```