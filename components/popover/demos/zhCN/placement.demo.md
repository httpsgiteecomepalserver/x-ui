# 弹出位置

```html
<x-space>
  <template v-for="placement in placements" :key="placement">
    <x-tooltip :title="placement" :placement="placement">
      <x-button>{{ placement }}</x-button>
    </x-tooltip>
  </template>
</x-space>
```

```js
export default {
  setup() {
    return {
      placements: ["left", "right", "top", "bottom", "topLeft", "leftTop", "topRight", "rightTop", "bottomRight", "rightBottom", "bottomLeft", "leftBottom"]
    }
  }
}
```