# 基础用法

试试点击灰色区域

```html
<x-space>
  <div v-sunken style="width: 300px; height: 300px; background: #d9d9d9;"></div>
  <div v-sunken style="width: 200px; height: 200px; background: #d9d9d9;"></div>
  <div v-sunken style="width: 100px; height: 100px; background: #d9d9d9;"></div>
</x-space>
```