import _Acrylic, { AcrylicDirective } from 'vue-acrylic'

const Acrylic = {
  ..._Acrylic,
  ...AcrylicDirective
}

export default Acrylic
