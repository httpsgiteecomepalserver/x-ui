# 亮度


```html
<x-space vertical style="max-width: 600px">
  <x-switch v-model:value="color" checked="亮色" unchecked="暗色" :checked-value="null" unchecked-value="#333333" />
  <x-space justify="space-evenly">
    <x-slider v-model:value="brightness1" :min=".5" :max="1.8" :step=".01" :format-tooltip="format" />
    <x-slider v-model:value="brightness2" :min=".5" :max="1.5" :step=".01" :format-tooltip="format" />
  </x-space>
  <div class="flex-center" :style="{color: !color ? '#333333' : '#fff'}">
    <div class="item" v-acrylic="{color, brightness: brightness1}">
      {{ format(brightness1) }}
    </div>
    <div class="item" v-acrylic="{color, brightness: brightness2}">
      {{ format(brightness2) }}
    </div>
  </div>
</x-space>
```

```js
export default {
  data: () => ({
    color: null,
    brightness1: 1.8,
    brightness2: 1,
    format: num => (num * 100).toFixed() + '%'
  })
}
```

```css
.flex-center {
  display: flex;
  justify-content: center;
  padding: 80px 30px;
  background: url(https://gitee.com/httpsgiteecomepalserver/x-ui/raw/master/images/wallpaper/img1.jpg?1) no-repeat 90%/250%;
}

.item {
  width: 200px;
  height: 80px;
  padding: 8px 12px;
}

.x-switch {
  display: flex;
  margin: 0 auto 20px;
}
.x-slider {
  width: 100px;
}
```