# 亚克力材料

```html
<x-form class="form" layout="vertical">
  <x-space class="bg">
    <div v-acrylic="{blur, opacity, brightness, saturate, noiseOpacity, noiseSize}" class="item"></div>
    <div v-acrylic="{blur, opacity, brightness, saturate, noiseOpacity, noiseSize}" class="item"></div>
    <div v-acrylic="{blur, opacity, brightness, saturate, noiseOpacity, noiseSize}" class="item"></div>
    <div v-acrylic="{blur, opacity, brightness, saturate, noiseOpacity, noiseSize}" class="item"></div>
  </x-space>
  <x-form-item :label="'blur: ' + blur">
    <x-slider v-model:value="blur" :max="50" />
  </x-form-item>
  <x-form-item :label="'opacity: ' + format(opacity)">
    <x-slider v-model:value="opacity" :max="1" :step=".01" :format-tooltip="format" />
  </x-form-item>
  <x-form-item :label="'brightness: ' + format(brightness)">
    <x-slider v-model:value="brightness" :max="2" :step=".01" :format-tooltip="format" />
  </x-form-item>
  <x-form-item :label="'saturate: ' + format(saturate)">
    <x-slider v-model:value="saturate" :max="3" :step=".01" :format-tooltip="format" />
  </x-form-item>
  <x-form-item :label="'noise-opacity: ' + format(noiseOpacity)">
    <x-slider v-model:value="noiseOpacity" :max="1" :step=".01" :format-tooltip="format" />
  </x-form-item>
  <x-form-item :label="'noise-size: ' + noiseSize">
    <x-slider v-model:value="noiseSize" />
  </x-form-item>
</x-form>
```

```js
export default {
  data: () => ({
    blur: 30,
    opacity: .6,
    brightness: 1,
    saturate: 1.6,
    noiseOpacity: .6,
    noiseSize: 30,

    format: num => (num * 100).toFixed() + '%'
  })
}
```

```css
.form {
  max-width: 600px;
}
.bg {
  display: flex;
  width: 100%;
  margin: 20px 0;
  padding: 120px 60px;
  box-sizing: border-box;
  background: url(https://gitee.com/httpsgiteecomepalserver/x-ui/raw/master/images/wallpaper/img100.jpg) no-repeat bottom/cover;
}
.item {
  width: 100px;
  height: 100px;
}
```