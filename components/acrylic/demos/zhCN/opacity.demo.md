# 透明度

```html
<x-space vertical style="max-width: 600px">
  <x-switch v-model:value="color" checked="亮色" unchecked="暗色" :checked-value="null" unchecked-value="#333333" />
  <div class="grid" :style="{color: !color ? '#333333' : '#fff'}">
    <div class="item item-1" v-acrylic="{color, opacity: .6}">
      60%
    </div>
    <div class="item" v-acrylic="{color, opacity: 1}">
      100%
    </div>
    <div class="item" v-acrylic="{color, opacity: .8}">
      80%
    </div>
    <div class="item" v-acrylic="{color, opacity: .4}">
      40%
    </div>
    <div class="item" v-acrylic="{color: '#ff5e00', opacity: .6}">
      60%
    </div>
  </div>
</x-space>
```

```js
export default {
  data: () => ({
    color: null
  })
}
```

```css
.grid {
  display: grid;
  grid-template-columns: 100px 100px 100px 100px;
  grid-template-rows: 180px 100px;
  gap: 15px;
  justify-content: center;
  padding: 60px;
  background: url(https://gitee.com/httpsgiteecomepalserver/x-ui/raw/master/images/wallpaper/img1.jpg) no-repeat center/cover;
}
.item {
  padding: 8px 12px;
}
.item-1 {
  grid-column-start: span 4;
}

.x-switch {
  display: flex;
  margin: 0 auto;
}
```