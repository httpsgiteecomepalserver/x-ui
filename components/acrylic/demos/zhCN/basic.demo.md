# 基础用法


```html
<x-space class="bg">
  <div v-acrylic class="item"></div>
  <div v-acrylic class="item"></div>
  <div v-acrylic class="item"></div>
</x-space>
```

```css
.bg {
  display: flex;
  max-width: 600px;
  padding: 60px;
  box-sizing: border-box;
  background: url(https://gitee.com/httpsgiteecomepalserver/x-ui/raw/master/images/wallpaper/LenovoWallPaper.jpg) no-repeat bottom/cover;
}
.item {
  width: 100px;
  height: 100px;
}
```