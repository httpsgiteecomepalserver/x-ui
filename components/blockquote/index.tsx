import { App, defineComponent } from "vue"

const prefixCls = 'x-blockquote'

const Blockquote = defineComponent({
  name: prefixCls,
  props: {

  },
  setup(_, { slots }) {
    return () => {
      return <blockquote class={prefixCls}>{ slots.default?.() }</blockquote>
    }
  }
})

Blockquote.install = (app: App) => {
  app.component(Blockquote.name, Blockquote)
}

export default Blockquote