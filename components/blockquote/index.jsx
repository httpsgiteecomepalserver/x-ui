import { defineComponent } from "vue";
const prefixCls = 'x-blockquote';
const Blockquote = defineComponent({
    name: prefixCls,
    props: {},
    setup(_, { slots }) {
        return () => {
            var _a;
            return <blockquote class={prefixCls}>{(_a = slots.default) === null || _a === void 0 ? void 0 : _a.call(slots)}</blockquote>;
        };
    }
});
Blockquote.install = (app) => {
    app.component(Blockquote.name, Blockquote);
};
export default Blockquote;
