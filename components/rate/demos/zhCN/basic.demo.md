# 基础用法

```html
<x-rate v-model:value="value" />
```

```js
export default {
    data() {
        return {
            value: 3
        }
    }
}
```