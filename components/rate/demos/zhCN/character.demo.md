# 自定义字符

```html
<x-space vertical>
    <x-rate character="⚔" :value="1" />
    <x-rate character="⚒" :value="2" />
    <x-rate character="㊣" :value="3" />

    <x-rate :value="4" >
      <template #character>
        <x-icon type="loading" :size="32" spin />
      </template>
    </x-rate>
</x-space>
```