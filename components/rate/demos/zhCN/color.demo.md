# 颜色

```html
<x-space vertical>
    <x-rate character="☭" :value="1" color="#18a058" />
    <x-rate character="⚝" :value="2" color="#f0a020" />
    <x-rate character="☣" :value="3" color="#d03050" />
</x-space>
```