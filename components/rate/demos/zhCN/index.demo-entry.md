# 评分 Rate

```demo
basic
color
count
allow-half
disabled
character
```

### Props

| 名称 | 类型 | 默认值 | 说明 |
| --- | --- | --- | --- |
| disabled | `boolean` | `false` | 是否禁用 |
| character | `string \| slot` | `-` | 自定义字符 |
| allowHalf | `boolean` | `false` | 允许只激活一半图标 |
| color | `string` | `-` | 已激活图标颜色 |
| count | `number` | `5` | 图标个数 |
| value(v-model) | `number` | `-` | 当前值 |

### Event

| 名称 | 回调参数 | 说明 |
| --- | --- | --- |
| change | `(value: number)` | 	选择时的回调 |
| hoverChange | `(value: number)` | 	鼠标经过时数值变化的回调 |