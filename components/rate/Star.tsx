import { defineComponent, reactive, watch, inject, computed, getCurrentInstance, PropType, VNodeChild } from "vue"

export default defineComponent({
  inheritAttrs: false,
  props: {
    prefixCls: String,
    value: Number,
    index: Number,
    count: Number,
    allowHalf: Boolean,
    disabled: Boolean,
    character: [String, Number, Object] as PropType<VNodeChild>,
    color: String,
    onClick: Function,
    onHover: Function,
  },
  setup(props, { emit }) {
    const { proxy: self } = getCurrentInstance()

    function onClick(e: MouseEvent) {
      emit('click', e, props.index)
    }
    function onHover(e: MouseEvent) {
      emit('hover', e, props.index)
    }
    function onKeydown(e: KeyboardEvent) {
      if (e.key === 'Enter') {
        emit('click', e)
      }
    }

    const isHalf = computed(() => props.allowHalf && (props.value>props.index && props.value<props.index+1),)
    const isActive = computed(() => props.value >= props.index+1)

    const cls = computed(() => {
      const { prefixCls } = props
      return [prefixCls, {
        [`${prefixCls}-half`]: isHalf.value,
        [`${prefixCls}-active`]: isActive.value
      }]
    })
    
    return () => {
      const { prefixCls, disabled, count, value, index, character } = props
      
      const style = { color: props.color }

      return (
        <div
          class={cls.value}
          style={isActive.value && style}
          onClick={disabled ? null : onClick}
          onKeydown={disabled ? null : onKeydown}
          onMousemove={disabled ? null : onHover}
          role="radio"
          aria-checked={value > index ? 'true' : 'false'}
          tabindex={disabled ? -1 : 0}
        >
          <div class={`${prefixCls}_1`} style={isHalf.value && style}>{character}</div>
          <div class={`${prefixCls}_2`} style={isActive.value && style}>{character}</div>
        </div>
      )
    }
  }
})