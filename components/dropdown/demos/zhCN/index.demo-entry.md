# 下拉菜单

```demo
basic
menu
placement
contextmenu
```

### Props

| 名称             | 类型                                                       | 默认值         | 说明         |
| ---------------- | ---------------------------------------------------------- | -------------- | ------------ |
| visible(v-model) | `boolean`                                                  | `false`        | 是否显示     |
| popup            | `string \| slot`                                           | `-`            | 弹出内容     |
| disabled         | `boolean`                                                  | `false`        | 是否禁用     |
| placements       | `'topLeft' \| 'topRight' \| 'bottomRight' \| 'bottomLeft'` | `'bottomLeft'` | 菜单弹出位置 |
| trigger          | `'click' \| 'contextmenu' \| 'focus' \| 'hover'`           | `'hover'`      | `-`          |
| popup-class      | `string`                                                   | `-`            |              |
| popup-style      | `object`                                                   | `-`            |              |

### Event

| 名称          | 回调参数             | 说明 |
| ------------- | -------------------- | ---- |
| visibleChange | `(visible: boolean)` | `-`  |
