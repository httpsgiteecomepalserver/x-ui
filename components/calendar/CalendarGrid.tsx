import { Ref } from 'vue'
import Grid, { GridProps } from './Grid'

type Props = GridProps & Pick<Required<GridProps>, 'prefixCls' | 'start' | 'modelValue' | 'rangeValue' | 'onSelect' | 'onHover' | 'disabled' | 'onGroupChange'> & { _ref?: Ref }

// ================================ DateGrid ================================

export const dateProps = {
  add: (date, i) => date.clone().add(i, 'days'),
  diff: (d1, d2) => d1.diff(d2, 'days'),
  isBefore: (d1, d2) => d1.isBefore(d2, 'days'),
  isAfter: (d1, d2) => d1.isAfter(d2, 'days'),
  isSame: (d1, d2) => d1.isSame(d2, 'days'),
  //
  group: date => date.clone().date(1),
  addGroup: (date, i) => date.clone().add(i, 'months'),
  isBeforeGroup: (d1, d2) => d1.isBefore(d2, 'months'),
  isAfterGroup: (d1, d2) => d1.isAfter(d2, 'months'),
  cell: date => date.date()
} as GridProps

export const DateGrid = (props: Props) => (
  <Grid
    //
    ref={props._ref}
    class={`${props.prefixCls}_grid ${props.prefixCls}_container`}
    cols={7}
    onHover={props.onHover}
    {...dateProps}
    {...props}
  />
)

// ================================ MonthGrid ================================

export const monthProps = {
  add: (date, i) => date.clone().add(i, 'months'),
  diff: (d1, d2) => d1.diff(d2, 'months'),
  isBefore: (d1, d2) => d1.isBefore(d2, 'months'),
  isAfter: (d1, d2) => d1.isAfter(d2, 'months'),
  isSame: (d1, d2) => d1.isSame(d2, 'months'),
  //
  group: date => date.clone().month(0).date(1),
  addGroup: (date, i) => date.clone().add(i, 'years'),
  isBeforeGroup: (d1, d2) => d1.isBefore(d2, 'years'),
  isAfterGroup: (d1, d2) => d1.isAfter(d2, 'years'),
  cell: date => date.month() + 1 + '月'
} as GridProps

export const MonthGrid = (props: Props) => (
  <Grid
    //
    ref={props._ref}
    class={`${props.prefixCls}_grid ${props.prefixCls}_container`}
    cols={4}
    onHover={props.onHover}
    {...monthProps}
    {...props}
  />
)

// ================================ YearGrid ================================

export const yearProps = {
  add: (date, i) => date.clone().add(i, 'years'),
  diff: (d1, d2) => d1.diff(d2, 'years'),
  isBefore: (d1, d2) => d1.isBefore(d2, 'years'),
  isAfter: (d1, d2) => d1.isAfter(d2, 'years'),
  isSame: (d1, d2) => d1.isSame(d2, 'years'),
  // prettier-ignore
  group: date => date.clone().add(-date.year() % 10, 'years').month(0).date(1),
  addGroup: (date, i) => date.clone().add((-date.year() % 10) + i * 10, 'years'),
  isBeforeGroup: (d1, d2) => d1.year() < d2.year() - (d2.year() % 10),
  isAfterGroup: (d1, d2) => d1.year() > d2.year() - (d2.year() % 10) + 9,
  cell: date => date.year()
} as GridProps

export const YearGrid = (props: Props) => (
  <Grid
    //
    ref={props._ref}
    class={`${props.prefixCls}_grid ${props.prefixCls}_container`}
    cols={4}
    onHover={props.onHover}
    {...yearProps}
    {...props}
  />
)
