import { App } from 'vue'
import { DateCalendar as Calendar } from './Calendar'

export { DateCalendar as Calendar, DateCalendar, MonthCalendar, YearCalendar } from './Calendar'
export { default as RangeCalendar } from './RangeCalendar'

Calendar.install = (app: App) => {
  app.component(Calendar.name, Calendar)
}

export default Calendar
