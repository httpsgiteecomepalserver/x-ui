# 日历 Calendar

> 日历是虚拟列表，可无限滚动

<br />

<!--single-column-->

```demo
basic
cell
disabled
range
```

## Props

| 名称           | 类型                            | 默认值 | 说明           |
| -------------- | ------------------------------- | ------ | -------------- |
| value(v-model) | `Moment`                        | `-`    | 展示日期       |
| range          | `[Moment, Moment]`              | `-`    | 展示日期范围   |
| disabled-date  | `(date: Moment) => boolean`     | `-`    | 禁止日期       |
| date-cell      | `#date-cell="{ date: Moment }"` | `-`    | 自定义日期渲染 |

## Event

| 名称   | 回调参数         | 说明             |
| ------ | ---------------- | ---------------- |
| select | `(date: Moment)` | 点击选择日期回调 |
| hover  | `(date: Moment)` | `hover` 日期回调 |
