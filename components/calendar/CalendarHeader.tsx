import { defineComponent, PropType } from "vue"
import { Moment } from 'moment'

import Icon from "../icon"

export default defineComponent({
  props: {
    prefixCls: String,
    value: Object as PropType<Moment>,
    title: String
  },
  emits: ['change', 'panelChange', 'prev', 'next'],
  setup(props, { attrs, emit }) {

    function prev() {
      emit('prev')
    }
    function next() {
      emit('next')
    }
    function showMonthPanel() {
      emit('panelChange')
    }
    
    return () => {
      const { prefixCls, value } = props

      return (
        <div class={`${prefixCls}_header`}>
          <span class={`${prefixCls}_header_title`} onClick={showMonthPanel}>{props.title || value.format('YYYY年M月')}</span>
          <span class={`${prefixCls}_prev-btn`} onClick={prev}><Icon type="arrow-up" /></span>
          <span class={`${prefixCls}_next-btn`} onClick={next}><Icon type="arrow-down" /></span>
        </div>
      )
    }
  }
})