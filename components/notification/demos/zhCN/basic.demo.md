# 基础用法

```html
<x-space>
  <x-button type="primary" @click="info">show</x-button>
</x-space>
```

```js
import { Notification } from 'x-ui-vue3'

export default {
  setup() {
    return {
      info() {
        const hide = Notification.create({
          title: '启用 Windows 防火墙',
          description: 'Windows 防火墙已关闭。单击或点击启用。',
          meta: '20:48',
          onClick: () => hide()
        })
      }
    }
  }
}
```
