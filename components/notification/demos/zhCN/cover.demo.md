# 封面

```html
<x-space>
  <x-button type="primary" @click="info">show</x-button>
</x-space>
```

```js
import { h } from 'vue'
import { Notification } from 'x-ui-vue3'

export default {
  setup() {
    return {
      info() {
        const hide = Notification.create({
          cover: h('div', { style: { height: '300px', width: '100%', background: 'url(https://i.postimg.cc/W4SnWQxL/download.jpg) center 10%/cover no-repeat' } }),
          title: '灰之魔女',
          description: '史上最年轻的魔女',
          onClick: () => hide()
        })
      }
    }
  }
}
```
