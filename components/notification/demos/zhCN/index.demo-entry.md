# 全局通知 Notification

```demo
basic
cover
duration
update
destroy
```

### API

- `Notification.create(option: NotificationOption)`
- `Notification.destroyAll()`

### NotificationOption

| 名称        | 类型               | 默认值  | 说明                 |
| ----------- | ------------------ | ------- | -------------------- |
| cover       | `VNodeTypes`       | `-`     | 封面                 |
| title       | `VNodeTypes`       | `-`     | 标题                 |
| description | `VNodeTypes`       | `-`     | 描述                 |
| meta        | `VNodeTypes`       | `-`     |                      |
| closable    | `boolean`          | `false` | 是否显示 close 图标  |
| duration    | `number`           | `5000`  | 显示时长，0 为不关闭 |
| icon        | `VNodeTypes`       | `-`     | 信息图标             |
| key         | `string \| number` | `-`     | 唯一标志             |
