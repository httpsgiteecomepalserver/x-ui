# duration

延时 `10s`

```html
<x-button type="primary" @click="info">信息</x-button>
```

```js
import { h } from 'vue'
import { Notification } from 'x-ui-vue3'

export default {
  setup() {
    const text = `²³³³³³³　　6666666666　　　太好了
23333　　NBNB     ²³³³³³³³³³³　　　⁶⁶⁶⁶⁶⁶
　 ²³³³³³³　    666　　太牛了！！　　 ²³³³³³³　⁶⁶⁶⁶⁶⁶ 　　厉害了　        　 ²³³³³³³³³³³
　 66666　       　太厉害了　　 66666　　 ²³³³³³³                  ⁶⁶⁶⁶⁶`

    return {
      info() {
        Notification.create({
          title: h('pre', { style: { fontFamily: 'inherit' } }, text),
          duration: 10000
        })
      }
    }
  }
}
```
