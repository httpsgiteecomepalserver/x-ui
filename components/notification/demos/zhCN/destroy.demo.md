# destroy

自行移除。

```html
<x-button type="primary" @click="request">request</x-button>
```

```js
import { Notification } from 'x-ui-vue3'

export default {
  setup() {
    return {
      request() {
        const hide = Notification.create({
          description: '你知道 《妮可冒险记》 吗？',
          duration: 0
        })
        setTimeout(hide, 3000)
      }
    }
  }
}
```
