# 更新内容

```html
<x-space vertical>
  通过 key 来更新内容
  <x-button @click="updateByKey">🍟🍔🥛</x-button>
  通过响应式数据来更新内容
  <x-button type="primary" @click="updateByReactive">🍟🍔🥛</x-button>
</x-space>
```

```js
import { ref } from 'vue'
import { Notification } from 'x-ui-vue3'

export default {
  setup() {
    return {
      updateByKey() {
        const key = 1
        Notification.create({ title: '🍟🍔🥛', description: '1 + 1 = 12', key })
        setTimeout(() => {
          Notification.create({ title: '🍔🍔🍔', description: '双层吉士汉堡', key })
        }, 1500)
      },
      updateByReactive() {
        const title = ref('🍟🍔🥛')
        const description = ref('1 + 1 = 12')
        setTimeout(() => {
          title.value = '🍔🍔🍔'
          description.value = '双层吉士汉堡'
        }, 1500)
        Notification.create({ title: () => title.value, description: () => description.value })
      }
    }
  }
}
```
