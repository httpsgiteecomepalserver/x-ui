import { App } from 'vue'
import Image from './Image'

Image.install = (app: App) => {
  app.component(Image.name, Image)
}

export default Image
