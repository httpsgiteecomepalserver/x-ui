# WIN10 案例

```html
<x-space vertical>
  <span style="font-size: 28px; line-height: 1.8em;">背景</span>
  <x-image width="328" height="206" :src="list[index]" :fit="fit" placeholder v-sunken />

  <br />

  <span>选择图片</span>
  <x-space size="4">
    <x-image v-for="(item, i) in list" :src="item" width="76" height="76" fit="cover" v-sunken @click="index = i" />
  </x-space>
  <x-button style="width: 90px">浏览</x-button>

  <br />

  <span>选择契合度</span>
  <x-select v-model:value="fit" :options="options" style="width: 280px;" />

  <br />

</x-space>
```

```js
export default {
  data: () => ({
    index: 0,
    list: [
      'https://gitee.com/httpsgiteecomepalserver/x-ui/raw/master/images/wallpaper/LenovoWallPaper.jpg',
      'https://gitee.com/httpsgiteecomepalserver/x-ui/raw/master/images/wallpaper/img1.jpg',
      'https://gitee.com/httpsgiteecomepalserver/x-ui/raw/master/images/wallpaper/img2.jpg',
      'https://gitee.com/httpsgiteecomepalserver/x-ui/raw/master/images/wallpaper/img3.jpg',
      'https://gitee.com/httpsgiteecomepalserver/x-ui/raw/master/images/wallpaper/img4.jpg',
    ],
    options: ["contain", "cover", "fill", "none", "scale-down"].map(value => ({ value })),
    fit: "cover"
  })
}
```