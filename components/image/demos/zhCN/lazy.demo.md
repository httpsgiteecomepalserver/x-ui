# 懒加载

可通过 `lazy` 开启懒加载功能

```html
<x-space vertical :wrap="false" style="height: 500px; overflow: auto;">
  <x-image v-for="item in list" :src="item" fit="cover" style="width: 100%; min-height: 300px;" lazy />
</x-space>
```

```js
export default {
  data: () => ({
    list: [
      'https://gitee.com/httpsgiteecomepalserver/x-ui/raw/master/images/wallpaper/img100.jpg',
      'https://gitee.com/httpsgiteecomepalserver/x-ui/raw/master/images/wallpaper/img101.png',
      'https://gitee.com/httpsgiteecomepalserver/x-ui/raw/master/images/wallpaper/img102.jpg',
      'https://gitee.com/httpsgiteecomepalserver/x-ui/raw/master/images/wallpaper/img103.png',
      'https://gitee.com/httpsgiteecomepalserver/x-ui/raw/master/images/wallpaper/img104.jpg',
    ]
  })
}
```