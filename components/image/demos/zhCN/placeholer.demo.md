# 占位内容

大图可使用 `placeholder`，支持以下几种类型

```html
<x-space>
  <x-space vertical align="center">
    <x-tag>boolean</x-tag>
    <x-image width="128" height="128" :src="src + random" placeholder />
  </x-space>
  <x-space vertical align="center">
    <x-tag>string</x-tag>
    <x-image width="128" height="128" :src="src + random" placeholder="😴" />
  </x-space>
  <x-space vertical align="center">
    <x-tag>slot</x-tag>
    <x-image width="128" height="128" :src="src + random">
      <template #placeholder>
        <x-icon type="loading" size="2em" spin />
      </template>
    </x-image>
  </x-space>
</x-space>
<x-button type="primary" @click="random = Date.now()">reload</x-button>
```

```js
export default {
  data: () => ({
    src: 'https://gitee.com/httpsgiteecomepalserver/x-ui/raw/master/images/wallpaper/img1.jpg?',
    random: 1
  })
}
```