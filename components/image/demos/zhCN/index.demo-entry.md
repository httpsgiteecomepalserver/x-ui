# 图像 Image

```demo
basic
placeholer
error
lazy
win10
```

## Props

| 名称 | 类型 | 默认值 | 说明 |
| --- | --- | --- | --- |
| src | `string` | `-` | 图片地址	 |
| width | `string \| number` | `-` | 图像宽度 |
| height | `string \| number` | `-` | 图像高度 |
| fit | `string` | `-` | [object-fit](https://developer.mozilla.org/en-US/docs/Web/CSS/object-fit) |
| alt | `string` | `-` | 图像描述 |
| placeholder | `boolean \| string \| slot` | `-` | 加载占位, 为 true 时使用默认占位 |
| fallback | `string` | `-` | 图片加载失败时显示的地址 |
| error | `slot` | `-` | 图片加载失败时显示的内容 |
| lazy | `boolean` | `false` | 是否开启懒加载 |


## Event

| 名称 | 回调参数 | 说明 |
| --- | --- | --- |
| load | `(e: Event)` | 图片加载成功的回调 |
| error | `(e: Event)` | 图片加载失败的回调 |