# 基础用法

```html
<x-space vertical>
  同步设置
  <x-switch v-model:value="checked" checked="开" unchecked="关" />
</x-space>
```

```js
export default {
    data() {
        return {
            checked: false
        }
    }
}
```