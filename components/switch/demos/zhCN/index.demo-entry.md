# 开关 Switch

```demo
basic
children
noop
disabled
```

### Props

| 名称 | 类型 | 默认值 | 说明 |
| --- | --- | --- | --- |
| disabled | `boolean` | `false` | 是否禁用 |
| checked | `string \| slot` | `-` | 选中时的内容 |
| unchecked | `string \| slot` | `-` | 非选中时的内容 |
| checkedValue | `	boolean \| string \| number` | `true` | 选中时的值 |
| uncheckedValue | `	boolean \| string \| number` | `false` | 	非选中时的值 |
| value(v-model) | `checkedValue \| uncheckedValue` | `false` | 是否选中 |

### Event

| 名称 | 回调参数 | 说明 |
| --- | --- | --- |
| change | `(value: checkedValue \| uncheckedValue)` | 输入框内容变化时的回调 |