# 禁用

```html
<x-space vertical>
  <x-switch :disabled="checked" />
  <x-switch v-model:value="checked" />
</x-space>
```

```js
export default {
  data() {
    return {
      checked: true
    }
  }
}
```