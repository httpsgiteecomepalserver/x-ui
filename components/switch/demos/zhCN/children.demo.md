# 文字

```html
<x-space vertical>

  <x-switch checked="😊" unchecked="🙃" />

  <x-switch>
    <template #checked>
      <x-icon type="check-circle" color="#0078d7" :size="18" />
    </template>
    <template #unchecked>
      <x-icon type="close-circle" color="#d03050" :size="18" />
    </template>
  </x-switch>
  
</x-space>
```