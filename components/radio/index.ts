import type { App } from 'vue'
import Radio from './Radio'
import RadioGroup from './Group'

export { default as Radio } from './Radio'
export { default as RadioGroup } from './Group'

Radio.install = (app: App) => {
  app.component(Radio.name, Radio)
  app.component(RadioGroup.name, RadioGroup)
}

export default Radio