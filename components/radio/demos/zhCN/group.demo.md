# 选项组

```html
<x-radio-group v-model:value="value">
  <x-radio value="1">🍌香蕉</x-radio>
  <x-radio value="2">🍉西瓜</x-radio>
  <x-radio value="3">🍋柠檬</x-radio>
</x-radio-group>
```

```js
export default {
  data() {
    return {
      value: '1'
    }
  }
}
```
