# 垂直

```html
<x-radio-group v-model:value="value" :options="options" vertical />
```

```js
export default {
  data() {
    return {
      value: '🍫巧克力',

      options: ['🍭棒棒糖', '🍦圆筒冰淇淋', '🍫巧克力'].map(val => ({
        label: val,
        value: val,
        style: { marginBottom: '10px' }
      }))
    }
  }
}
```