# 单选框 Radio

```demo
basic
group
options
vertical
disabled
```

### Radio Props

| 名称 | 类型 | 默认值 | 说明 |
| --- | --- | --- | --- |
| checked(v-model) | `boolean` | `false` | 是否选中 |
| value | `	string` | `-` | 选中的值 |
| disabled | `boolean` | `false` | 是否禁用 |

### RadioGroup Props

| 名称 | 类型 | 默认值 | 说明 |
| --- | --- | --- | --- |
| value(v-model) | `string` | `-` | 选中的值 |
| options | `Array` | `-` |   |
| name | `string` | `-` |   |
| vertical | `boolean` | `false` | 是否垂直 |
| disabled | `boolean` | `false` | 是否禁用 |

### RadioGroup Event

| 名称 | 回调参数 | 说明 |
| --- | --- | --- |
| change | `(value: string)` | 值变化时的回调函数 |