# 基础用法

```html
<x-space>
  <x-radio :checked="value == '🍌'" value="🍌" @change="onChange">🍌香蕉</x-radio>
  <x-radio :checked="value == '🍉'" value="🍉" @change="onChange">🍉西瓜</x-radio>
  <x-radio :checked="value == '🍋'" value="🍋" @change="onChange">🍋柠檬</x-radio>
</x-space>
```

```js
export default {
  data() {
    return {
      value: null
    }
  },
  methods: {
    onChange(val) {
      this.value = val
    }
  }
}
```
