import moment, { Moment, MomentInput } from 'moment'

export function formatDate(date: Moment | Moment[], format: string) {
  if (!date) return ''

  const formatFunc = (date: Moment) => date?.format(format)
  if (Array.isArray(date)) {
    return date.map(e => formatFunc(e))
  } else {
    return formatFunc(date)
  }
}

export function toMoment(date: MomentInput | MomentInput[], format: string, strict?: boolean) {
  if (!date) return null

  const formatFunc = (date: MomentInput) => moment(date, format, strict)
  if (Array.isArray(date)) {
    return date.map(e => formatFunc(e))
  } else {
    return formatFunc(date)
  }
}
