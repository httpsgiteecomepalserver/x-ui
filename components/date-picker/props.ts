import { PropType } from 'vue'
import { Arrayable } from '@vueuse/core'
import { Moment, MomentInput } from 'moment'

export type PickerType = 'year' | 'month' | 'date' | 'daterange' | 'monthrange'

export const PickerProps = {
  value: [String, Number, Object, Array] as PropType<Arrayable<MomentInput>>,
  type: { type: String as PropType<PickerType>, default: 'date' },
  clearable: { default: true },
  disabled: Boolean,
  disabledDate: Function as PropType<(date: Moment) => boolean>,
  placeholder: { type: [String, Array] as PropType<Arrayable<string>>, default: '选择日期' },
  format: { default: 'YYYY-MM-DD' },
  valueFormat: String
}
