# 日期范围

```html
<x-date-picker v-model:value="value" type="daterange" />
```

```js
export default {
  data() {
    return {
      value: [new Date(), new Date()]
    }
  }
}
```
