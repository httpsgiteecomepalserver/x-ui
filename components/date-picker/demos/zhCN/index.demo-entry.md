# 选择器 Select

```demo
basic
range
disabled-date
disabled
```

### Props

| 名称           | 类型                        | 默认值         | 说明                                                                    |
| -------------- | --------------------------- | -------------- | ----------------------------------------------------------------------- |
| value(v-model) | `moment \| moment[]`        | `-`            | 绑定的值                                                                |
| clearable      | `boolean`                   | `true`         | 是否可清空                                                              |
| disabled       | `boolean`                   | `false`        | 是否禁用                                                                |
| disabled-date  | `(date: Moment) => boolean` | `-`            | 禁用特定日期                                                            |
| placeholder    | `string \| string[]`        | `-`            | 输入框提示文字                                                          |
| format         | `string`                    | `"YYYY-MM-DD"` | 展示的日期格式，配置参考 [moment.js](https://momentjs.com/)             |
| value-format   | `string`                    | `Moment`       | 绑定值的格式 [具体格式](https://momentjs.com/docs/#/displaying/format/) |

### Event

| 名称   | 回调参数                      | 说明               |
| ------ | ----------------------------- | ------------------ |
| change | `(value: moment \| moment[])` | 值变化时的回调函数 |
