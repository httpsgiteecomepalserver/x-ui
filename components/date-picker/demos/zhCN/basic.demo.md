# 基础用法

```html
<x-date-picker v-model:value="value" />
```

```js
export default {
  data() {
    return {
      value: new Date()
    }
  }
}
```
