# 禁用特定日期

```html
<x-space vertical>
  禁用每月15日
  <x-date-picker :disabled-date="disabledDate1" />
  禁用未来日期
  <x-date-picker :disabled-date="disabledDate2" type="daterange" />
  禁用过去日期
  <x-date-picker :disabled-date="disabledDate3" />
</x-space>
```

```js
export default {
  setup() {
    return {
      disabledDate1(date) {
        return date.date() == 15
      },
      disabledDate2(date) {
        return date.isAfter(new Date(), 'day')
      },
      disabledDate3(date) {
        return date.isBefore(new Date(), 'day')
      }
    }
  }
}
```
