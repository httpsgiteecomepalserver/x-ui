# 禁用

```html
<x-space vertical>
  <x-switch v-model:value="disabled" />
  <x-date-picker :disabled="disabled" />
  <!-- <x-range-picker :disabled="disabled" /> -->
</x-space>
```

```js
export default {
  data() {
    return {
      disabled: true
    }
  }
}
```
