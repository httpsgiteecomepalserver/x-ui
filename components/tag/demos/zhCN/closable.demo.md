# 可关闭

```html
<x-tag closable @close="onClose">可关闭</x-tag>
```

```js
import { getCurrentInstance } from 'vue'

export default {
  setup() {
    const { proxy: vm } = getCurrentInstance()

    return {
      onClose() {
        vm.$message.success('白菜 哟！')
      }
    }
  }
}
```