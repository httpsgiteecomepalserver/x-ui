# 基础用法

```html
<x-space>
  <template v-for="type in types" :key="type">
    <x-tag :type="type">{{ type }}</x-tag>
  </template>
</x-space>
```

```js
export default {
  setup() {
    return {
      types: ["default", "primary", "info", "success", "warning", "error"]
    }
  }
}
```