# 圆角

```html
<x-space>
  <x-tag round size="small">史莱姆</x-tag>
  <x-tag round type="primary" size="medium">yyds</x-tag>
  <x-tag round type="error" size="large">禁止好耶 !</x-tag>
</x-space>
```