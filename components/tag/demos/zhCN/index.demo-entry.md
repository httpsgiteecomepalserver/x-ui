# 标签 Tag

```demo
basic
closable
size
round
```

### Props

| 名称 | 类型 | 默认值 | 说明 |
| --- | --- | --- | --- |
| closable | `boolean` | `false` | 是否可关闭 |
| closeIcon | `VNodeTypes` | `-` | 自定义关闭按钮 |
| round | `boolean` | `false` | 是否圆角 |
| bordered | `boolean` | `false` | 是否有边框 |
| nowrap | `boolean` | `false` | 不换行 |
| size | `'small' \| 'medium' \| 'large'` | `'small'` | 尺寸 |
| type | `'default' \| 'primary' \| 'info' \| 'success' \| 'warning' \| 'error'` | `'default'` | 类型 |