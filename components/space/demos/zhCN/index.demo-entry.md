# 间距 Space

<!--single-column-->

```demo
basic
vertical
size
customized-size
justify
```

### Props

| 名称 | 类型 | 默认值 | 说明 |
| --- | --- | --- | --- |
| size | `'small' \| 'medium' \| 'large' \| number \| [number, number]` | `'medium'` | 是否禁用 |
| vertical | `boolean` | `false` | 是否垂直布局 |
| wrap | `boolean` | `true` | 是否超出换行 |
| justify | `string` | `-` | 主轴对其方式 |
| align | `string` | `-` | 副轴对其方式 |
| item-style | `any` | `-` | 子元素样式 |