# duration

延时 `10s`

```html
<x-button type="primary" @click="info">信息</x-button>
```

```js
import { Message } from 'x-ui-vue3'

export default {
  setup() {
    return {
      info() {
        Message.info('信息', { duration: 10000 })
      }
    }
  }
}
```
