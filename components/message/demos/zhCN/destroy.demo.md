# destroy

自行移除。

```html
<x-button type="primary" @click="request">request</x-button>
```

```js
import { Message } from 'x-ui-vue3'

export default {
  setup() {
    return {
      request() {
        const hide = Message.loading('前方高能……', { duration: 0 })
        setTimeout(hide, 5000)
      }
    }
  }
}
```
