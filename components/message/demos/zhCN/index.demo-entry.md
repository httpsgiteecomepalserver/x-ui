# 全局提示 Message

```demo
basic
duration
update
destroy
```

### API

- `Message.success(content: ContentType, option: MessageOption)`
- `Message.error(content: ContentType, option: MessageOption)`
- `Message.info(content: ContentType, option: MessageOption)`
- `Message.warning(content: ContentType, option: MessageOption)`
- `Message.loading(content: ContentType, option: MessageOption)`

> 销毁

- `Message.destroyAll()`

### ContentType

```ts
type ContentType = string | (() => VNodeChild)
```

### MessageOption

| 名称     | 类型                               | 默认值  | 说明                 |
| -------- | ---------------------------------- | ------- | -------------------- |
| closable | `boolean`                          | `false` | 是否显示 close 图标  |
| duration | `number`                           | `3000`  | 显示时长，0 为不关闭 |
| icon     | `VNodeChild \| (() => VNodeChild)` | `-`     | 信息图标             |
| key      | `string \| number`                 | `-`     | 唯一标志             |
