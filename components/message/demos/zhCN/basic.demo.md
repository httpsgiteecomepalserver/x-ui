# 基础用法

```html
<x-space>
  <x-button type="primary" @click="info">信息</x-button>
  <x-button type="danger" @click="error">错误</x-button>
  <x-button type="warning" @click="warning">警告</x-button>
  <x-button type="success" @click="success">成功</x-button>
  <x-button type="primary" @click="loading">加载中</x-button>
</x-space>
```

```js
import { Message } from 'x-ui-vue3'

export default {
  setup() {
    return {
      info() {
        Message.info('信息')
      },
      error() {
        Message.error('错误')
      },
      warning() {
        Message.warning('警告')
      },
      success() {
        Message.success('成功')
      },
      loading() {
        Message.loading('加载中……')
      }
    }
  }
}
```
