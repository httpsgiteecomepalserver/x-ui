# 更新内容

```html
<x-space vertical>
  通过 key 来更新内容
  <x-button @click="updateByKey">update by key</x-button>
  通过响应式数据来更新内容
  <x-button type="primary" @click="updateByReactive">update by reactive</x-button>
</x-space>
```

```js
import { ref } from 'vue'
import { Message } from 'x-ui-vue3'

export default {
  setup() {
    const textRef = ref('')

    return {
      updateByKey() {
        const key = 1
        Message.loading('Loading...', { key })
        setTimeout(() => {
          Message.success('Loaded!', { key })
        }, 1000)
      },
      updateByReactive() {
        textRef.value = '~ 洁白 ~ '
        setTimeout(() => {
          textRef.value += '闪耀 ~ '
        }, 1000)
        setTimeout(() => {
          textRef.value += '奇迹之花 ~ '
        }, 2000)
        setTimeout(() => {
          textRef.value += 'White Lily 🍦~ '
        }, 3000)
        Message.info(() => textRef.value, { duration: 6000 })
      }
    }
  }
}
```
