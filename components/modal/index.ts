import { App, h } from 'vue'
import Icon from '../icon'
import Modal, { prefixCls } from "./Modal"
import ModalInfo, { destroyFns } from './ModalInfo'


function info(config) {
  return ModalInfo({
    ...config,
    cancel: false,
    icon: h(Icon, { type: 'info-circle', class: `${prefixCls}_icon-info` })
  })
}
function confirm(config) {
  return ModalInfo({
    ...config,
    icon: h(Icon, { type: 'check-circle', class: `${prefixCls}_icon-check` })
  })
}

Modal.info = info
Modal.confirm = confirm
Modal.destroyAll = () => destroyFns.forEach(fn => fn())

Modal.install = (app: App) => {
  app.component(Modal.name, Modal)
}

export default Modal