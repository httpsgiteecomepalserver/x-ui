# 模态框 Modal

```demo
basic
footer
footer-custom
btn-custom
api
async-close
```

## API

- `Modal.confirm(props): `
- `Modal.info(props)`
- `Modal.destroyAll()`

## Props

| 名称             | 类型                    | 默认值  | 说明                                 |
| ---------------- | ----------------------- | ------- | ------------------------------------ |
| visible(v-model) | `boolean`               | `false` | 是否可见                             |
| title            | `string \| slot`        | `-`     | 标题                                 |
| width            | `number`                | `512`   | 宽度                                 |
| body-style       | `object`                | `-`     | 内容样式                             |
| footer           | `boolean \| slot`       | `false` | 是否显示页脚                         |
| ok               | `boolean \| slot`       | `true`  | 是否显示确认按钮，footer=true 时有效 |
| cancel           | `boolean \| slot`       | `true`  | 是否显示取消按钮，footer=true 时有效 |
| mask             | `boolean`               | `true`  | 是否显示遮罩                         |
| mask-closable    | `boolean`               | `true`  | 遮罩可关闭                           |
| closable         | `boolean`               | `true`  | 是否显示右上角的关闭按钮             |
| esc              | `boolean`               | `true`  | 是否 esc 关闭                        |
| to               | `string \| HTMLElement` | `body`  | Modal 的挂载位置                     |
| before-ok        | `() => Promise`         | `-`     | 确认按钮是否自动关闭                 |

## Menu Event

| 名称       | 回调参数             | 说明             |
| ---------- | -------------------- | ---------------- |
| change     | `(visible: boolean)` |                  |
| ok         | `()`                 | 点击确认         |
| cancel     | `()`                 | 点击取消         |
| afterLeave | `()`                 | 关闭动画结束回调 |
| maskClick  | `()`                 | 遮罩点击         |
