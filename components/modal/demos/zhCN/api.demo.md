# Api

通过静态方法全局调用

```html
<x-space>
  <x-button type="primary" @click="info">Info</x-button>
  <x-button type="success" @click="confirm">Confirm</x-button>
</x-space>
```

```js
import { Modal } from 'x-ui-vue3'
export default {
  setup() {
    return {
      info() {
        Modal.info({
          title: 'Info',
          content: 'info info info'
        })
      },
      confirm() {
        Modal.confirm({
          title: 'Confirm',
          content: 'confirm confirm confirm'
        })
      }
    }
  }
}
```