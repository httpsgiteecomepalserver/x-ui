# 自定义页脚

`😆` 使用插槽 `#footer` 自定义页脚

```html
<x-switch v-model:value="visible" checked="😆" />
<x-modal v-model:visible="visible" title="🦎啊啊">
  <span style="font-size: 10em;">{{ text }}</span>
  <template #footer>
    <span style="font-size: 1.5em;">😁😎😀😁😂🤣😃😄😅😆</span>
  </template>
</x-modal>
```

```js
export default {
  data() {
    return {
      visible: false,
      text: '😃'
    }
  },
  created() {
    let boo = true
    this.interval = setInterval(() => {
      boo = !boo
      this.text = boo ? '😃' : '😆'
    }, 250)
  },
  destoryed() {
    clearInterval(this.interval)
  }
}
```