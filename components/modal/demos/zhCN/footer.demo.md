# 页脚

使用 `footer` 开启页脚

```html
<x-switch v-model:value="visible" />
<x-modal v-model:visible="visible" title="title" bodyStyle="height: 100px;" footer>
  <p>aasad</p>
  <p>aasad</p>
  <p>aasad</p>
  <p>aasad</p>
  <p>aasad</p>
  <p>aasad</p>
  <p>aasad</p>
</x-modal>
```

```js
export default {
  data() {
    return {
      visible: false
    }
  }
}
```