# 基础用法

```html
<x-switch v-model:value="visible" />
<x-modal v-model:visible="visible" title="Title">
  <p>content</p>
</x-modal>
```

```js
export default {
  data() {
    return {
      visible: false
    }
  }
}
```