# 自定义按钮

使用插槽 `#cancel` 或 `#ok` 自定义按钮

```html
<x-switch v-model:value="visible" />
<x-modal v-model:visible="visible" title="Title" footer>
  <template #cancel>
    <x-button ghost dashed>CANCEL</x-button>
  </template>
  <template #ok>
    <x-button type="primary" ghost>OK</x-button>
  </template>
</x-modal>
```

```js
export default {
  data() {
    return {
      visible: false
    }
  }
}
```