# 基础用法

使用指令 `v-border-light` 开启边缘高亮

```html
<x-space vertical style="width: 600px">

  <br/>
  <x-switch v-border-light />
  
  <x-rate v-border-light="{size: 1}" :value="2" />

  <br/>
  <x-space>
    <x-tag v-border-light="{size: 3}" size="large" type="warning">好耶！！！</x-tag>
    <x-tag v-border-light="{size: 1}" size="large" type="success">禁止好耶！！！</x-tag>
    <x-tag v-border-light="{size: 2}" size="large" type="error">好耶！！！</x-tag>
  </x-space>
  <x-space>
    <x-tag v-border-light="{size: 2}" size="large" type="error">好耶！！！</x-tag>
    <x-tag v-border-light="{size: 3}" size="large" type="warning">禁止好耶！！！</x-tag>
    <x-tag v-border-light="{size: 1}" size="large" type="success">好耶！！！</x-tag>
  </x-space>
  <x-space>
    <x-tag v-border-light="{size: 1}" size="large" type="success">好耶！！！</x-tag>
    <x-tag v-border-light="{size: 2}" size="large" type="error">禁止好耶！！！</x-tag>
    <x-tag v-border-light="{size: 3}" size="large" type="warning">好耶！！！</x-tag>
  </x-space>
  
  <x-tag v-border-light size="large">哥哥考上了公务员，妹妹接管家里生意，朋友创业发家致富，我当上了超市促销员：我们都有光明的未来!</x-tag>

  <br/>
  <x-tag v-border-light="{size: 8}" size="large" type="info">某个地方有一位旅人，她的名字是伊蕾娜。是一位年纪轻轻就成了魔法使中最上位「魔女」的天才。因为向往着幼时读过的旅行故事，随意地进行着漫长的旅行。在这个广阔的世界里自由地漫步，接触着形形色色有趣的人，体味着人们美好的日常生活，她作为一名旅人，不带有任何目的地接触着各种国家的各色人群。还有同样数量的——「不必理会我。我只是一介旅人罢了。接下来还得继续前往下一个地方呢。」由魔女伊蕾娜所连接的，关于相遇和离别的故事……。!</x-tag>

  <x-tag v-border-light size="large" type="success">今天一定要捉弄高木同学，让她害羞！!</x-tag>

  <br/>
  <x-tag v-border-light="{size: 1}" size="large">请问您今天要来点兔子吗？？!</x-tag>

</x-space>
```