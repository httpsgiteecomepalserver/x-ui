import { Plugin, Directive } from 'vue'
import RevealEffect, { RevealEffectDirective } from 'vue-reveal-effect'
import { name } from './interface'

RevealEffect.setDefaultProps({
  borderWidth: 2,
  borderColor: (el: HTMLElement) => window.getComputedStyle(el).getPropertyValue('--x-reveal-bc'),
  bgColor: (el: HTMLElement) => window.getComputedStyle(el).getPropertyValue('--x-reveal-bgc')
})

const BorderLight: Plugin & Directive & { name: string } = {
  ...RevealEffectDirective,
  name,
  install(app) {
    app.directive(name, RevealEffectDirective)
    RevealEffect.install(app)
  }
}

export default BorderLight
