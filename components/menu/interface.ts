import { VNodeChild } from 'vue'

export type Value = string | number

export interface MenuOptionBase {
  label: VNodeChild
  value?: Value
  type?: string
  disabled?: boolean
  icon?: VNodeChild | (() => VNodeChild)
  children?: Option[]
}

export interface MenuGroupOptionBase extends MenuOptionBase {
  type: 'group'
  children: Option[]
}

export type MenuOption = MenuOptionBase

export type MenuGroupOption = MenuGroupOptionBase

export type Option = MenuOption | MenuGroupOption
