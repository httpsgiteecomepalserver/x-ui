
# 菜单 Menu

```demo
horizontal
collapse
```

### Menu Props

| 名称 | 类型 | 默认值 | 说明 |
| --- | --- | --- | --- |
| collapsed | `boolean` | `false` | 菜单是否折叠，值在菜单为垂直时有用 |
| default-expand-all | `boolean` | `false` | 是否展开全部菜单 |
| expanded-keys(v-model) | `Array<string>` | `-` | 展开的子菜单标识符数组 |
| indent | `number` | `32` | 菜单缩进宽度 |
| options | `Array<MenuOption \| MenuGroupOption>` | `[]` | 菜单的数据 |
| mode | `'vertical' \| 'horizontal' \| ''inline''` | `'inline'` | 菜单的布局方式 |
| value(v-model) | `string` | `-` | 菜单当前的选中值 |

### Menu Event

| 名称 | 回调参数 | 说明 |
| --- | --- | --- |
| expandChange | `(keys: string[])` | 展开/关闭的回调 |
| select | `(item: MenuOption)` | 被选中时调用 |

### MenuOption

| 名称 | 类型 | 说明 |
| --- | --- | --- |
| label | `VNodeChild \| (() => VNodeChild)` | `-` |
| key | `string` | `-` |
| icon? | `() => VNode` | `-` |
| disabled? | `boolean` | `-` |
| children? | `Array<MenuOption \| MenuGroupOption>` | `-` |

### MenuGroupOption

| 名称 | 类型 | 说明 |
| --- | --- | --- |
| label | `VNodeChild \| (() => VNodeChild)` | `-` |
| key | `string` | `-` |
| type | `'group'` | `-` |
| children | `Array<MenuOption \| MenuGroupOption>` | `-` |
