import { defineComponent, ExtractPropTypes, InjectionKey, VNodeChild, PropType, provide, computed, Ref } from 'vue'
import { Value, MenuGroupOption, MenuOption } from './interface'
import { createItem, useMenu } from './utils'
import { omit } from '../_util/_'

import Trigger from '../trigger'
import MenuItemContent from './MenuItemContent'
import ExpandTransition from '../expand-transition'

export const submenuProps = {
  label: [String, Object] as PropType<string | VNodeChild>,
  value: [String, Number] as PropType<Value>,
  children: Array as PropType<Array<MenuOption | MenuGroupOption>>,
  icon: [Object, Function] as PropType<VNodeChild | (() => VNodeChild)>,
  disabled: Boolean
}

export type SubmenuProps = ExtractPropTypes<typeof submenuProps>

export type SubmenuContextProps = {
  props: SubmenuProps
  paddingLeftRef: Ref<number>
}

export const SubmenuContextKey: InjectionKey<SubmenuContextProps> = Symbol('submenu')

export default defineComponent({
  name: 'x-submenu',
  props: submenuProps,
  setup(props) {
    const { paddingLeftRef, menuContext, horizontalRef, inlineRef, isActiveRef, disabledRef, isRoot } = useMenu(props)

    const collapsedRef = computed(() => !menuContext.expandedKeysRef.value.includes(props.value))

    function onClick() {
      if (disabledRef.value || menuContext.collapsedRef.value || !inlineRef.value) return
      menuContext.onExpanded(props)
    }
    function onPopupVisibleChange(visible: boolean) {
      menuContext.onExpanded(props)
    }

    provide(SubmenuContextKey, {
      props,
      paddingLeftRef
    })

    return () => {
      const prefixCls = 'x-submenu'
      const horizontal = horizontalRef.value
      const inline = inlineRef.value
      const menuCollapsed = menuContext.collapsedRef.value
      const collapsed = collapsedRef.value

      const contentNode = (
        <MenuItemContent
          //
          {...omit(props, 'children')}
          showArrow={!menuCollapsed || !isRoot}
          collapsed={collapsed}
          style={`paddingLeft:${paddingLeftRef.value}px`}
          active={isActiveRef.value}
          onClick={onClick}
        />
      )

      // todo
      const childrenNode = (
        <ExpandTransition>
          <div v-show={!collapsed} class={`${prefixCls}_children`} role='menu'>
            {props.children.map(createItem)}
          </div>
        </ExpandTransition>
      )

      const popupNode = (
        <div class={`${prefixCls}_children`} role='menu'>
          {props.children.map(createItem)}
        </div>
      )

      return (
        <Trigger
          //
          prefixCls={prefixCls}
          disabled={disabledRef.value || (!(horizontal || menuCollapsed) ? inline : false)}
          action='hover'
          popup={popupNode}
          popupVisible={!collapsed}
          onPopupVisibleChange={onPopupVisibleChange}
          popupClass={`${prefixCls}-popup`}
          popupPlacement={horizontal && isRoot ? 'bottom' : 'rightTop'}
        >
          <div class={prefixCls} role='menuitem' aria-expanded={!collapsed} aria-disabled={disabledRef.value}>
            {contentNode}
            {!(menuCollapsed || horizontal || !inline) && childrenNode}
          </div>
        </Trigger>
      )
    }
  }
})
