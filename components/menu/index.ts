import { App } from 'vue'
import Menu from './Menu'

Menu.install = (app: App) => {
  app.component(Menu.name, Menu)
}

export default Menu