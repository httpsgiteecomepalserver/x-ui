import { defineComponent, VNodeChild, PropType, provide } from 'vue'
import { Value, MenuGroupOption, MenuOption } from './interface'
import { createItem, useMenu } from './utils'

import { SubmenuContextKey } from './Submenu'

export const submenuProps = {
  node: Object as PropType<MenuOption>,
  label: [String, Object] as PropType<string | VNodeChild>,
  value: [String, Number] as PropType<Value>,
  children: Array as PropType<Array<MenuOption | MenuGroupOption>>,
  icon: [Object, Function] as PropType<VNodeChild | (() => VNodeChild)>,
  disabled: Boolean
}

export default defineComponent({
  name: 'x-menu-item-group',
  props: submenuProps,
  setup(props) {
    const { paddingLeftRef, parentPaddingLeftRef } = useMenu(props)

    provide(SubmenuContextKey, {
      props,
      paddingLeftRef: parentPaddingLeftRef
    })

    return () => {
      const prefixCls = 'x-menu-item-group'

      const childrenNode = <div class={`${prefixCls}_children`}>{props.children.map(createItem)}</div>

      return (
        <div class={prefixCls}>
          <div class={`${prefixCls}_label`} style={`padding-left:${paddingLeftRef.value}px`}>
            {props.label}
          </div>
          {childrenNode}
        </div>
      )
    }
  }
})
