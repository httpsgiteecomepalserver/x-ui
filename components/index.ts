import type { App } from 'vue'

import * as components from './components'
export * from './components'

const install = (app: App) => {
  Object.keys(components).forEach(key => {
    const component = components[key]
    if (component.install) {
      // console.log(`========================${component.name}`)
      app.use(component)
    }
  })
  return app
}

export default {
  install
}
