import type { App } from 'vue'
import Checkbox from './Checkbox'
import Group from './Group'

export { default as Checkbox } from './Checkbox'
export { default as CheckboxGroup } from './Group'

Checkbox.install = (app: App) => {
  app.component(Checkbox.name, Checkbox)
  app.component(Group.name, Group)
}

export default Checkbox
