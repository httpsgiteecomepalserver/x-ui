# 选项组

```html
<x-checkbox-group v-model:value="value">
  <x-checkbox value="1">🍌香蕉</x-checkbox>
  <x-checkbox value="2">🍉西瓜</x-checkbox>
  <x-checkbox value="3">🍋柠檬</x-checkbox>
</x-checkbox-group>
```

```js
export default {
  data() {
    return {
      value: ['1']
    }
  }
}
```
