# 禁用

```html
<x-space vertical>
  <x-checkbox-group v-model:value="value" :options="options" disabled />
  <x-checkbox-group v-model:value="value" :options="options" />
</x-space>
```

```js
export default {
  data() {
    return {
      value: ['🍭棒棒糖', '🍫巧克力', '🍓草莓'],

      options: ['🍭棒棒糖', '🍦圆筒冰淇淋', '🍫巧克力', '🥙夹心饼', '🍺啤酒', '🍓草莓'].map((val, i) => ({
        label: val,
        value: val,
        disabled: i % 2
      }))
    }
  }
}
```
