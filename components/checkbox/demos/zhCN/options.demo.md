# options

```html
<x-checkbox-group v-model:value="value" :options="options" />
```

```js
export default {
  data() {
    return {
      value: [1],

      options: [
        { value: 1, label: '🍭棒棒糖' },
        { value: 2, label: '🍦圆筒冰淇淋' },
        { value: 3, label: '🍫巧克力' }
      ]
    }
  }
}
```
