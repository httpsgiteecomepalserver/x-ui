# 基础用法

```html
<x-checkbox v-model:checked="value">自动连接</x-checkbox>
```

```js
export default {
  data() {
    return {
      value: true
    }
  }
}
```
