# 大小保持比例

使用 `aspect-ratio` 控制大小是否保持比例

```html
<x-space vertical>
  <x-switch v-model:value="aspectRatio" checked="true" unchecked="false" />
  <x-drag-resize :initW="100" :initH="100" style="background: #87d068;" :aspect-ratio="aspectRatio" />
</x-space>
```

```js
export default {
  data: () => ({
    aspectRatio: true
  })
}
```