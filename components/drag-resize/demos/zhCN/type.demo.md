# 简单类型

设置 `type="simple"` 将不显示节点

```html
<x-space vertical>
  <x-drag-resize :initW="100" :initH="100" style="background: #108ee9;">
    <span style="background: #ffffff4f">default</span>
  </x-drag-resize>
  <x-drag-resize :initW="100" :initH="100" type="simple" style="background: #83b3fb;">
    <span style="background: #ffffff4f">simple</span>
  </x-drag-resize>
</x-space>
```