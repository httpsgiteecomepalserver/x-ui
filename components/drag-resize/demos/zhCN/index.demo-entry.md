# 拖拽

```demo
basic
type
aspect-ratio
custom
drag-target
other
```

## Props

| 名称            | 类型                    | 默认值      | 说明               |
| --------------- | ----------------------- | ----------- | ------------------ |
| active(v-model) | `boolean`               | `false`     | 是否激活           |
| x(v-model)      | `number`                | `-`         | x                  |
| y(v-model)      | `number`                | `-`         | y                  |
| w(v-model)      | `number`                | `-`         | 宽度               |
| h(v-model)      | `number`                | `-`         | 高度               |
| init-w          | `number`                | `-`         | 初始化宽度         |
| init-h          | `number`                | `-`         | 初始化高度         |
| min-w           | `number`                | `-`         | 最小宽度           |
| min-h           | `number`                | `-`         | 最小高度           |
| max-w           | `number`                | `-`         | 最大宽度           |
| max-h           | `number`                | `-`         | 最大高度           |
| type            | `'default' \| 'simple'` | `'default'` | 风格               |
| resizing-class  | `any`                   | `-`         | 调整大小时的 class |
| resizing-style  | `StyleValue`            | `-`         | 调整大小时的 style |
| active-class    | `any`                   | `-`         | 激活状态的 class   |
| active-style    | `StyleValue`            | `-`         | 激活状态的 style   |
| stick-class     | `any`                   | `-`         | 句柄的 class       |
| stick-style     | `StyleValue`            | `-`         | 句柄的 style       |
| aspect-ratio    | `boolean`               | `false`     | 是否保持其比例     |
| draggable       | `boolean`               | `true`      | 是否可拖动         |
| resizable       | `boolean`               | `true`      | 是否可调整大小     |
| drag-target     | `HtmlElement`           | `-`         | 可进行拖拽的目标   |
