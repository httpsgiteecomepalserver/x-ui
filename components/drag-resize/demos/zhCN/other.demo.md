# 更多属性

```html
<x-space vertical>
  <x-space size="30">
    <x-form-item label="draggable">
      <x-switch v-model:value="draggable" />
    </x-form-item>
    <x-form-item label="resizable">
      <x-switch v-model:value="resizable" />
    </x-form-item>
    <x-form-item label="active">
      <x-switch v-model:value="active" />
    </x-form-item>
  </x-space>
  <x-drag-resize
    :initW="100"
    :initH="100"
    :draggable="draggable"
    :resizable="resizable"
    :active="active"
    style="background: #87d068;"
  />
</x-space>
```

```js
export default {
  data: () => ({
    draggable: true,
    resizable: true,
    active: true,
  })
}
```