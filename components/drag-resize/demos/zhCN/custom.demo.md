# 自定义样式

`stick-style` 自定义节点的样式
<br/>
<br/>
`active-style` 自定义激活时的样式

```html
<x-drag-resize
  :initW="100"
  :initH="100"
  style="background: #fd7433"
  stick-style="width: 15px; height: 15px; border-radius: 15px; background: #87d068"
  active-style="box-shadow: 0 0 15px -5px"
/>
```