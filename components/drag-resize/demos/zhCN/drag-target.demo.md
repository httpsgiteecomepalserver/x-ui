# 拖拽目标

使用 `drag-target` 设置可拖拽的目标元素

```html
<x-drag-resize
  :initW="100"
  :initH="100"
  :drag-target="dragTarget"
  style="display: flex; align-items: center; justify-content: center; background: #21cbc2;"
>
  <button ref="dragTarget" style="background: #ffffff80">点我拖拽</button>
</x-drag-resize>
```

```js
import { ref } from 'vue'

export default {
  setup() {
    return {
      dragTarget: ref()
    }
  }
}
```