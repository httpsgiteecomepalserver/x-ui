# WIN10 案例

```html
<div vertical style="max-width: 600px">
  <span style="font-size: 22px; line-height: 2em;">Windows-SSD (C:) - 100GB</span> 
  <x-progress :percent="90" color="#c50500" :show-text="false" :stroke-width="16" />
  <x-space justify="space-between" style="color: #666666;">
    <span>已使用95.8GB</span>
    <span>4.14GB可用</span>
  </x-space>
  <span style="line-height: 2em;">存储空间的使用情况和可用情况如下。</span>
  
  <div v-for="item in list" style="display: flex; align-items: center; padding: 2px 10px 10px 0;">
    <x-icon :type="item.icon" size="28" color="#0078d7" style="margin-right: 15px" />
    <div style="flex: 1;">
      <x-space justify="space-between" style="line-height: 2;">
        <span>{{ item.title }}</span>
        <span>{{ item.remain }}</span>
      </x-space>
      <x-progress :percent="item.percent" :show-text="false" />
      <span style="line-height: 1.4; display: flex; font-size: 12px; color: #666666;">{{ item.tip }}</span>
    </div>
  </div>

</div>
```

```js
export default {
  data: () => ({
    list: [
      { percent: 45, title: '应用和功能', tip: '卸载未使用或不需要的应用和功能', remain: '38.2 GB', icon: 'screen' },
      { percent: 8, title: '其他', tip: '管理其他大文件夹', remain: '5.25 GB', icon: 'other' },
      { percent: 4, title: '临时文件', tip: '选择要删除的临时文件', remain: '2.77 GB', icon: 'delete' },
      { percent: 2, title: '视频', tip: '管理“视频”文件夹', remain: '1.49 GB', icon: 'video' },
      { percent: 2, title: '文档', tip: '管理“文档”文件夹', remain: '1.35 GB', icon: 'folder' },
    ]
  })
}
```