# 基础用法

```html
<x-switch v-model:value="loading" checked="Loading……" unchecked="点击加载" />
<x-space v-skeleton="loading">
  <span v-skeleton-item>Vue (读音 /vjuː/，类似于 view) 是一套用于构建用户界面的渐进式框架。</span>
  <span v-skeleton-item>与其它大型框架不同的是，Vue 被设计为可以自底向上逐层应用。</span>
  <span v-skeleton-item>Vue 的核心库只关注视图层，不仅易于上手，还便于与第三方库或既有项目整合。</span>
  <span v-skeleton-item>当与现代化的工具链以及各种支持类库结合使用时，Vue 也完全能够为复杂的单页应用提供驱动。</span>
</x-space>
```

```js
export default {
  data: () => ({
    loading: true,
  })
}
```