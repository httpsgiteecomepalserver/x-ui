# Loading 状态

```html
<x-space vertical :wrap="false">
  <x-switch v-model:value="loading" checked="Loading……" unchecked="点击加载" />
  <x-space style="overflow: auto;" :wrap="false">

    <x-card v-skeleton="loading" style="width: 200px" hoverable>
      <template #cover>
        <div v-skeleton-item style="height: 225px; background: url(http://t13.baidu.com/it/u=1523492823,4065692774&fm=224&app=112&f=JPEG?w=337&h=500) top/cover no-repeat;" />
      </template>
      <template #title>
        <div class="title" v-skeleton-item>让你赚大钱</div>
      </template>
      <template #header-extra>
        <div class="extra" v-skeleton-item>:</div>
      </template>
      <div class="content" v-skeleton-item>你现在的工作能做一辈子？老板会给你退休金？万一重病，医疗保险金真的够用？因此，你非得有钱不可！</div>
      <template #footer>
        <span v-skeleton-item>4条评价</span>
      </template>
    </x-card>

    <x-card v-skeleton="loading" style="width: 200px" hoverable>
      <template #cover>
        <div v-skeleton-item style="height: 225px; background: url(https://img12.360buyimg.com/n1/jfs/t1/3214/31/19386/60912/62b1aa6aE2f21f169/b75a9a8cb6970675.jpg) center/cover no-repeat;" />
      </template>
      <template #title>
        <div class="title" v-skeleton-item>vue.js设计与实现</div>
      </template>
      <template #header-extra>
        <div class="extra" v-skeleton-item>:</div>
      </template>
      <div class="content" v-skeleton-item>本书基于Vue.js 3，从规范出发，以源码为基础，并结合大量直观的配图...</div>
      <template #footer>
        <span v-skeleton-item>10万+条评价</span>
      </template>
    </x-card>

    <x-card v-skeleton="loading" style="width: 200px" hoverable>
      <template #cover>
        <div v-skeleton-item style="height: 225px; background: url(https://tse1-mm.cn.bing.net/th/id/OIP-C.fL217KSjYmaHo78ZYlCDqAHaJa?pid=ImgDet&rs=1) top/cover no-repeat;" />
      </template>
      <template #title>
        <div class="title" v-skeleton-item>javascript高级程序设计第4四版</div>
      </template>
      <template #header-extra>
        <div class="extra" v-skeleton-item>:</div>
      </template>
      <div class="content" v-skeleton-item>本书是JavaScript经典图书的新版。第4版涵盖ECMAScript2019，全面、深入地介绍了...</div>
      <template #footer>
        <span v-skeleton-item>200+条评价</span>
      </template>
    </x-card>

  </x-space>
</x-space>
```

```js
export default {
  data: () => ({
    loading: true
  })
}
```

```css
.title {
  overflow: inherit;
  text-overflow: inherit;
}
.extra {
  margin-left: 4px;
  padding: 0;
  width: 28px;
  height: 28px;
  border-radius: 28px;
  font-weight: bold;
  text-align: center;
  background: #0000000d;
}
.content {
  overflow: hidden;
  text-overflow: ellipsis;
  display: -webkit-box;
  -webkit-line-clamp: 2;
  -webkit-box-orient: vertical;
}
:deep(.x-card_footer) {
  font-size: 12px;
}
```