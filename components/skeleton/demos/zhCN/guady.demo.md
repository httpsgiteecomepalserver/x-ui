# 花里胡哨

`v-skeleton` 的值可以是一个对象

```html
<div class="grid" v-skeleton.animated="obj">
  <x-switch v-model:value="obj.loading" checked="Loading……" unchecked="点击加载" />
  <x-space vertical>
    <img class="avatar" src="https://tse3-mm.cn.bing.net/th/id/OIP-C.deYn3qH6BxQobDWN6vZDgwAAAA?pid=ImgDet&rs=1" v-skeleton-item />
    <div v-skeleton-item>「洁白~」</div>
    <div v-skeleton-item>「闪耀~」</div>
    <div v-skeleton-item>「奇迹~」</div>
    <div v-skeleton-item>「之花~」</div>
  </x-space>
  <x-space vertical>
    <span v-skeleton-item>——「心里感觉痒痒的」</span>
    <span v-skeleton-item>——「超级无敌可爱」</span>
    <span v-skeleton-item>——「铭印」</span>
    <span v-skeleton-item>——「方便说两句吗」</span>
    <span v-skeleton-item>——「好啦 交给我吧」</span>
    <span v-skeleton-item>——「有些事不知为妙」</span>
    <span v-skeleton-item>——「天使的目光」</span>
    <span v-skeleton-item>——「不会辜负期待啊」</span>
  </x-space>
</div>
```

```js
export default {
  data: () => ({
    obj: {
      loading: true,
      // 应用了 v-skeleton 节点加载中的 class
      loadingClass: "loadingClass",
      // 应用了 v-skeleton 节点加载中的 style
      loadingStyle: "box-shadow: 0 0 20px -14px #000",
      // 应用了 v-skeleton-item 节点加载中的 class
      class: "my-item",
      // 应用了 v-skeleton-item 节点加载中的 style
      style: "cursor: pointer"
    }
  })
}
```

```css
:deep(.my-item) {
  border-radius: 10px !important;
  transition: box-shadow 200ms;
}
:deep(.my-item:hover) {
  border: 4px #0000000d solid;
  box-shadow: 0 0 0 4px #0078d71a, 0 0 0 8px #0078d70d;
}

.grid {
  display: grid;
  grid-template-columns: auto 1fr;
  gap: 15px;
  padding: 8px;
}

.grid > :first-child {
  grid-column-start: span 2;
}

.loadingClass {
  border-radius: 15px;
  transition: box-shadow 200ms;
}

.loadingClass:hover {
  box-shadow: 0 0 20px -14px #000, 0 0 0 10px #0078d71a !important;
}

.avatar {
  width: 100px;
  height: 100px;
}

span {
  display: inline-block;
  padding: 4px 0;
  line-height: 1;
}
```