# 动画效果

使用修饰符 `animated` 开启动画效果

```html
<x-space v-skeleton.animated="loading" vertical style="max-width: 400px">
  <x-switch v-model:value="loading" checked="Loading……" unchecked="点击加载" />
  <span></span>
  <span v-skeleton-item>——「谢谢」</span>
  <span v-skeleton-item>——「你很努力了」</span>
  <span v-skeleton-item>——「最喜欢了」</span>
  <span></span>
  <span v-skeleton-item>—— 心怀着这三句话活下去</span>
  <span v-skeleton-item>—— 希望有一天能成为温柔的人</span>
</x-space>
```

```js
export default {
  data: () => ({
    loading: true,
  })
}
```