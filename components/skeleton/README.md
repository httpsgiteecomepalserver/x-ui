<h1 align="center">SKELETON</h1>
<p align="center">骨架屏</p>

<br><br>

## 文档

[@x-ui-vue3/skeleton](http://httpsgiteecomepalserver.gitee.io/x-ui/#/zh-CN/components/skeleton)

## 安装

使用 npm 安装

```bash
npm i -S @x-ui-vue3/skeleton
```

使用 yarn 安装

```bash
yarn add -S @x-ui-vue3/skeleton
```

##  示例
main.js
```js
import { createApp } from 'vue'
import Skeleton from '@x-ui-vue3/skeleton'
import App from './App.vue'

createApp(App).use(Skeleton).mount('#app')
```
App.vue
```html
<script setup>
import { ref } from 'vue'
const loading = ref(false)
</script>

<template>
  <label for="loading">点击切换</label>
  <input v-model="loading" id="loading" type="checkbox" />

  <br /><br />

  <div v-skeleton="loading">
    <span v-skeleton-item>超文本标记语言是一种用于创建网页的标准标记语言。</span>
    <br /><br />
    <span v-skeleton-item>www.runoob.com</span>
    <br /><br />
    <span v-skeleton-item>Good good study, day day up!</span>
  </div>
</template>
```

## 快速入门 DEMO
```bash
# clone the project
git clone https://github.com/huodoushigemi/example-skeleton.git

# enter the project directory
cd example-skeleton

# install dependency
npm install

# develop
npm run dev
```

## 效果图

<img src="https://user-images.githubusercontent.com/41646242/175770558-48044e18-5a77-47ab-ad5c-5767de5a007f.gif" width="80%" />

<img src="https://user-images.githubusercontent.com/41646242/175770724-9565fd39-1bd5-4da6-9df8-b7dba8dbe562.gif" width="80%" />

<img src="https://user-images.githubusercontent.com/41646242/175770649-9f67808a-5765-4455-8461-a843ce37ba1e.gif" width="80%" />

