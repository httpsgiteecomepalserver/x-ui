import { App } from 'vue'
import { name } from './interface'
import Skeleton from './skeleton'
import SkeletonItem from './skeleton-item'
import './style'

export { default as Skeleton } from './skeleton'
export { default as SkeletonItem } from './skeleton-item'

// @ts-ignore
Skeleton.install = (app: App) => {
  app.directive(name, Skeleton)
  app.directive(`${name}-item`, SkeletonItem)
}

export default Skeleton
