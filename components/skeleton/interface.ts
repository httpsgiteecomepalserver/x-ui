import { CSSProperties, DirectiveBinding, StyleValue, VNode, WatchStopHandle } from "vue"

export const 
  name = 'skeleton',
  preCls = `x-${name}`,
  itemPreCls = `${preCls}-item`,
  itemRefPrecls = `${itemPreCls}-ref`

export type BaseValue = {
  class?: any,
  style?: StyleValue
}

export type BaseDirectiveBinding = DirectiveBinding<BaseValue>

type BaseProps = {
  el: HTMLElement
  binding: BaseDirectiveBinding
}

export type Props = {
  fragment: HTMLElement
  targets?: ItemProps[],
  vnode: VNode<any, any>
  unwatch?: WatchStopHandle
} & BaseProps & {
  binding: DirectiveBinding<boolean | {
    loading: boolean
    loadingClass?: any
    loadingStyle?: StyleValue
  }>
}

export type ItemProps = {
  state: { style?: CSSProperties }
  resizeCb?: () => void
} & BaseProps