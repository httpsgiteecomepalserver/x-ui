import { normalizeClass, VNode, mergeProps as _mergeProps } from "vue"
import { preCls, Props } from "./interface"

export const patchClass = (el: HTMLElement, clazz, oldClazz) => {
  clazz = clazz.split(' '), oldClazz = oldClazz.split(' ')
  const delCls = oldClazz.filter(e => !clazz.includes(e)), addCls = clazz
  delCls.forEach(e => e && el.classList.remove(e))
  addCls.forEach(e => e && el.classList.add(e))
}

export const getLoading = (binding: Props['binding']) => typeof binding.value === 'object' ? binding.value.loading : binding.value

export const getAnimated = (binding: Props['binding']): boolean => binding.modifiers.animated

export const getClass = (binding: Props['binding']) => {
  const loading = getLoading(binding)
  if (!loading) return ''
  const animated = getAnimated(binding)
  // @ts-ignore
  return normalizeClass([preCls, { [`${preCls}-animated`]: animated }, binding.value.loadingClass])
}

export const allClass = (binding: Props['binding']) => {
  // @ts-ignore
  return normalizeClass([preCls, `${preCls}-animated`, binding.value.loadingClass])
}

export const fliterClass = (cls: string) => {
  return cls.split(' ').filter(cls => cls.includes(preCls)).join(' ')
}

export function mergeProps(vnode: VNode, binding: Props['binding']) {
  if (getLoading(binding)) {
    if (vnode.props) {
      const props = _mergeProps(vnode.props, {
        class: getClass(binding),
      })
      props.class = [...new Set((props.class as String).split(' '))].join(' ')
      Object.assign(vnode.props, props)
    } else {
      const clazz = normalizeClass([vnode.el.className, getClass(binding)])
      vnode.el.className = clazz
    }
  }
}