import { ComputedRef, InjectionKey, Ref } from "vue";
import { RuleItem, ValidateOption } from 'async-validator'

import { FormProps } from "./Form";
import { FormItemProps } from './FormItem'

export interface FormContextProps {
  props: FormProps
  addItem: (FormItemVm) => void
  delItem: (FormItemVm) => void
}

export const FormContextKey: InjectionKey<FormContextProps> = Symbol('FormContextKey')


export type FormItemVm = {
  props: FormItemProps
  valueRef: ComputedRef<any>
  resetField: () => void
  clearValidate: () => void
  validate: (options?: FormItemValidateOptions) => Promise<void>
}


export type FormRules = {
  [prop: string]: FormItemRule | FormItemRule[]
}

export type FormItemRule = {
  trigger?: ValidationTrigger
} & RuleItem

export type FormItemValidateOptions = {
  trigger?: ValidationTrigger
} & ValidateOption


export type ValidationTrigger = 'change' | 'blur' | 'focus'