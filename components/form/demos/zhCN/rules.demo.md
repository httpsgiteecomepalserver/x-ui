# 表单验证

>校验规则参见 [async-validator](https://github.com/yiminghe/async-validator#usage)

```html
<br/>
<x-form ref="formRef" :model="model" :rules="rules" style="max-width: 380px;">
  <x-form-item label="年龄" prop="age">
    <x-input v-model:value="model.age" />
  </x-form-item>
  <x-form-item label="密码" prop="password">
    <x-input v-model:value="model.password" />
  </x-form-item>
  <x-form-item label="重复密码" prop="reenteredPassword">
    <x-input v-model:value="model.reenteredPassword" />
  </x-form-item>
  <x-form-item>
    <x-space>
      <x-button type="primary" @click="onsubmit">提交</x-button>
      <x-button @click="reset">重置</x-button>
    </x-space>
  </x-form-item>
</x-form>
```

```js
import { reactive, ref } from 'vue'

export default {
  setup() {
    const formRef = ref()
    const model = reactive({
      age: null,
      password: null,
      reenteredPassword: null,
    })

    const rules = {
      age: {
        required: true,
        validator(rule, value) {
          return parseInt(value) > 18
        },
        message: '未成年人禁止体验🥰😂'
      },
      password: {
        required: true
      },
      reenteredPassword: {
        required: true,
        validator(rule, value) {
          return value == model.password
        },
        message: '两次密码输入不一致'
      }
    }

    const onsubmit = () => {
      formRef.value
        .validate()
        .then(() => {
          console.log('success')
        })
        .catch(error => {
          console.log('error', error)
        })
    }
    const reset = () =>  {
      formRef.value.resetFields()
    }

    return {
      formRef,
      model,
      rules,
      onsubmit,
      reset
    }

  }
}
```