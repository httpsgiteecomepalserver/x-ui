# 2021 哔哩哔哩站会员激活自选题

```html
<x-form ref="formRef" :model="model" :rules="rules" layout="vertical">
  <x-form-item label="奥特曼来自哪个星云？" prop="奥特曼" first>
    <x-radio-group v-model:value="model.奥特曼" :options="list.奥特曼" />
  </x-form-item>
  <x-form-item label="初音岛上的万年萝莉是谁？" prop="初音岛" first>
    <x-radio-group v-model:value="model.初音岛" :options="list.初音岛" />
  </x-form-item>
  <x-form-item label="《魔法少女小圆》的导演是谁？" prop="小圆" first>
    <x-radio-group v-model:value="model.小圆" :options="list.小圆" />
  </x-form-item>
  <x-form-item label="国际通用的标准音的频率是？" prop="频率" first>
    <x-radio-group v-model:value="model.频率" :options="list.频率" />
  </x-form-item>
  <x-form-item label="3Dmax中旋转视图按哪个键？" prop="max" first>
    <x-radio-group v-model:value="model.max" :options="list.max" />
  </x-form-item>
  <br/>
  <x-form-item>
    <x-space>
      <x-button type="primary" @click="onsubmit">提交</x-button>
      <x-button @click="reset">重置</x-button>
    </x-space>
  </x-form-item>
</x-form>
```

```js
import { reactive, ref } from 'vue'

export default {
  setup() {
    const formRef = ref()
    const model = reactive({
      奥特曼: null,
      初音岛: null,
      小圆: null,
      频率: null,
      max: null
    })

    const rules = {
      奥特曼(rule, value) {
        console.log(rule)
        if (value != 'm78') return new Error('错误！')
      },
      初音岛(rule, value) {
        if (value != '芳乃樱') return new Error('错误！')
      },
      小圆(rule, value) {
        if (value != '新房昭之') return new Error('错误！')
      },
      频率(rule, value) {
        if (value != '440Hz') return new Error('错误！')
      },
      max(rule, value) {
        if (value != 'alt+鼠标中键') return new Error('错误！')
      },
    }

    const onsubmit = () => {
      formRef.value
        .validate()
        .then(() => {
          console.log('success')
        })
        .catch(error => {
          console.log('error', error)
        })
    }
    const reset = () =>  {
      formRef.value.resetFields()
    }

    return {
      formRef,
      model,
      rules,
      onsubmit,
      reset,

      list: {
        奥特曼: ['m10', 'm78', 'm97', 'm79'].map(e => ({ label: e, value: e })),
        初音岛: ['森园立夏', '芳乃樱', '白河', '小鸟'].map(e => ({ label: e, value: e })),
        小圆: ['虚渊玄', '新房昭之', '新海诚', '宫崎骏'].map(e => ({ label: e, value: e })),
        频率: ['261Hz', '20000Hz', '440Hz', '20Hz'].map(e => ({ label: e, value: e })),
        max: ['space+右键', 'alt+鼠标中键', '鼠标左键', 'ctrl+左键'].map(e => ({ label: e, value: e }))
      }
    }

  }
}
```