# 只使用 FormItem

你可以单独使用 `x-form-item`

```html
<div style="max-width: 280px;">
  <x-form-item label="姓名" prop layout="vertical">
    <x-input v-model:value="model.name" />
  </x-form-item>
  <x-form-item label="年龄" prop layout="vertical" :rules="ageRules">
    <x-slider v-model:value="model.age" />
  </x-form-item>
</div>
```

```js
import { reactive } from 'vue'

export default {
  setup() {
    const model = reactive({
      name: null,
      age: 22
    })

    const ageRules = {
      trigger: 'change',
      validator() {
        return model.age >= 18
      },
      message: '我铐，这也太刑了吧，这种可狱不可囚的斩新身活就在阎前啊，小日子越过越有判头了'
    }

    return {
      model,
      ageRules
    }

  }
}
```