# 基础用法

```html
<x-form ref="formRef" :model="model" layout="vertical" style="max-width: 280px;">
  <x-form-item label="更改内置显示器的亮度" prop="light">
    <x-slider v-model:value="model.light" />
  </x-form-item>
  <x-form-item label="夜间模式" prop="dark">
    <x-switch v-model:value="model.dark" checked="开" unchecked="关" />
  </x-form-item>
  <x-form-item label="更改文本、应用等项目的大小" prop="scale">
    <x-select v-model:value="model.scale" :options="scales" />
  </x-form-item>
  <x-form-item label="显示分辨率" prop="screen">
    <x-select v-model:value="model.screen" :options="screens" />
  </x-form-item>
  <x-form-item label="显示方向" prop="direction">
    <x-select v-model:value="model.direction" :options="directions" />
  </x-form-item>
  <br />
  <x-form-item>
    <x-space>
      <x-button type="primary" @click="onsubmit">提交</x-button>
      <x-button @click="reset">重置</x-button>
    </x-space>
  </x-form-item>
</x-form>
```

```js
import { reactive, ref } from 'vue'

export default {
  setup() {
    const formRef = ref()
    const model = reactive({
      light: 70,
      dark: true,
      scale: '150%',
      screen: '2560 × 1600',
      direction: '横向'
    })

    const onsubmit = () => {
      formRef.value
        .validate()
        .then(() => {
          console.log('success')
        })
        .catch(error => {
          console.log('error', error)
        })
    }
    const reset = () => {
      formRef.value.resetFields()
    }

    return {
      formRef,
      model,
      onsubmit,
      reset,

      scales: ['100%', '125%', '150%', '175%', '200%'].map(e => ({ label: e, value: e })),
      screens: ['2560 × 1600', '1920 × 1080', '1920 × 1200', '1680 × 1050', '1600 × 900', '1440 × 900', '1280 × 800', '1280 × 768', '1280 × 720', '1280 × 600'].map(e => ({ label: e, value: e })),
      directions: ['横向', '纵向'].map(e => ({ label: e, value: e }))
    }
  }
}
```
