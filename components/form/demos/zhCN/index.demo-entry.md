# 表单 Form

```demo
basic
layout
rules
bilibili2021
form-item
```

### Form Props

| 名称 | 类型 | 默认值 | 说明 |
| --- | --- | --- | --- |
| model | `object` | `-` | 表单数据对象 |
| rules | `object` | `-` | [表单验证规则](https://github.com/yiminghe/async-validator#usage) |
| layout | `'horizontal' \| 'vertical'` | `'horizontal'` | 表单布局 |
| label-width | `number` | `-` |  标签的宽度 |

### Form Methods
| 名称 | 	参数 | 说明 |
| --- | --- | --- |
| validate | `() => Promise` | 触发表单验证 |
| validateFields | `(nameList: string[]) => Promise` | 触发表单验证 |
| clearValidate | `-` | 移除表单项的校验结果 |
| resetFields | `-` | 对整个表，将所有字段值重置为初始值并移除校验结果单进行重置 |

<!-- ### Event

| 名称 | 回调参数 | 说明 |
| --- | --- | --- |
| change | `(value: moment \| moment[])` | 值变化时的回调函数 | -->


### FormItem Props
| 名称 | 类型 | 默认值 | 说明 |
| --- | --- | --- | --- |
| label | `string \| slot` | `-` | 标签 |
| label-width | `number` | `-` | 标签的宽度 |
| layout |`'horizontal' \| 'vertical'` | `-` | 布局 |
| prop | `string` | `-` | 表单域 model 字段，支持 `'a.b.c'`格式 |
| rules | `object \| Array` | `-` | [表单验证规则](https://github.com/yiminghe/async-validator#usage) |
| required | `boolean` | `false` | 是否必填 |
| first | `boolean` | `false` | 是否只展示首个出错信息 |
| extra | `string \| slot` | `-` | 额外的提示信息 |