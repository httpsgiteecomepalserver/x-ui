# 表单布局

```html
<x-form ref="formRef" :model="model" :layout="model.layout" style="max-width: 380px;">
  <x-form-item label="布局">
    <x-radio-group v-model:value="model.layout">
      <x-radio value="horizontal">Horizontal</x-radio>
      <x-radio value="vertical">Vertical</x-radio>
    </x-radio-group>
  </x-form-item>
  <x-form-item label="姓名" prop="name">
    <x-input v-model:value="model.name" />
  </x-form-item>
  <x-form-item label="年龄" prop="age">
    <x-slider v-model:value="model.age" />
  </x-form-item>
  <x-form-item label="电话号码" prop="phone">
    <x-input v-model:value="model.phone" />
  </x-form-item>
  <x-form-item label="出生" prop="birth">
    <x-date-picker v-model:value="model.birth" value-format="YYYY-MM-DD" />
  </x-form-item>
  <x-form-item>
    <x-space>
      <x-button type="primary" @click="onsubmit">提交</x-button>
      <x-button @click="reset">重置</x-button>
    </x-space>
  </x-form-item>
</x-form>
```

```js
import { reactive, ref } from 'vue'

export default {
  setup() {
    const formRef = ref()
    const model = reactive({
      layout: 'horizontal',
      name: null,
      age: 22,
      phone: null,
      birth: null,
    })

    const onsubmit = () => {
      formRef.value
        .validate()
        .then(() => {
          console.log('success')
        })
        .catch(error => {
          console.log('error', error)
        })
    }
    const reset = () =>  {
      formRef.value.resetFields()
    }

    return {
      formRef,
      model,
      onsubmit,
      reset
    }

  }
}
```