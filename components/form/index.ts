import { App } from 'vue'

import Form from "./Form";
import FormItem from "./FormItem";

export { default as Form } from './Form'
export { default as FormItem } from './FormItem'

Form.install = (app: App) => {
  app.component(Form.name, Form)
  app.component(FormItem.name, FormItem)
}

export default Form