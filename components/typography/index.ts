import { App } from 'vue'
import A from './a.vue'
import SimpleTable from './simple-table.vue'

export { default as A } from './a.vue'
export { default as SimpleTable } from './simple-table.vue'
