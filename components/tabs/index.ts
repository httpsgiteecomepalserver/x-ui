import { App } from 'vue'
import TabPane from './TabPane'
import Tabs from './Tabs'

export { default as TabPane } from './TabPane'
export { default as Tabs } from './Tabs'

Tabs.install = (app: App) => {
  app.component(Tabs.name, Tabs)
  app.component(TabPane.name, TabPane)
}

export default Tabs
