import { ComponentPublicInstance } from 'vue'
import { Tab } from './Tabbar'
import { TabPaneProps } from './TabPane'

export function normalize(o: Tab[number]) {
  return typeof o === 'object'
    ? {
        label: o.label ?? o.name,
        name: o.name ?? o.label
      }
    : { label: o, name: o }
}

export function getKey(tab: ComponentPublicInstance<TabPaneProps>) {
  if (!tab) return
  return tab.name ?? tab.label
}
