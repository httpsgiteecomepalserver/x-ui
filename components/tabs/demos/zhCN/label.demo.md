# 自定义标签页

可以通过插槽 `#label` 来实现自定义标签页的内容

```html
<x-tabs type="segment">
  <x-tab-pane>
    <template #label><x-icon type="iphone" />&nbsp;强得很</template>
    <x-tag size="large" type="error">虎气风发新一年，走⁠起。</x-tag>
    <br />
    <br />
    <x-tag size="large" type="primary">
      全球疫情迫使人们的日常都戴上了口罩，这对于要用Face ID解锁的iPhone用户来说就尤其不方便，不过今天，苹果正式推送了iOS/iPadOS 15.4首个开发者Beta版本，算是正式官方解决了这个问题。
      <br />
      <br />
      苹果在最新的15.4版本中，新增了戴口罩使用面容ID的功能，面容ID在设置为识别全脸面貌时最为准确，若要在戴口罩时使用面容ID，iPhone可识别眼睛周围独有特征进行认证。
      <br />
      <br />
      更新的方法为，如果你有付费且有效的苹果开发者账号，那么可以直接前往开发者中心下载。
      <br />
      <br />
      如果没有，那就需要安装开发者Beta的描述文件，另外提醒一下，这次iOS 15.4 Beta1的体积容量达到了5.4GB之大，升级还需谨慎。
      <br />
      <br />
      早先针对于戴口罩使用面容ID解锁，苹果的做法是使用Apple Watch可以解决，但这就等于说强制让人们再去购买一款产品了，这次的15.4更新官方终于意识到了这个问题并给出了解决方法。
    </x-tag>
    <br />
    <br />
    <x-tag>2022-01-28 23:03:42</x-tag>
  </x-tab-pane>
  <x-tab-pane>
    <template #label><x-icon type="iphone" />&nbsp;解锁超能力</template>
    <x-tag size="large">RMB 217/月起或RMB 5199 起，还可折抵换购1。</x-tag>
  </x-tab-pane>
  <x-tab-pane>
    <template #label><x-icon type="iphone" />&nbsp;哪款 iPhone 适合你?</template>
    <x-tag size="large">iPhone 13 Pro</x-tag>
  </x-tab-pane>
  <x-tab-pane>
    <template #label><x-icon type="iphone" />&nbsp;不一样</template>
    <x-tag size="large">为什么 iPhone 就是那么不一样？</x-tag>
    <br />
    <br />
    <x-tag>iOS 15 更有聊，更专注。</x-tag>
  </x-tab-pane>
  <x-tab-pane>
    <template #label><x-icon type="iphone" />&nbsp;隐私</template>
    <x-tag size="large">什么可以分享，理应由你做主。</x-tag>
  </x-tab-pane>
</x-tabs>
```