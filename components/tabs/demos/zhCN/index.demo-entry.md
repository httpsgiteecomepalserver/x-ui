# 标签页 Tabs

<!--single-column-->

```demo
basic
line
segment
card
border-card
justify
label
leave
```

### Tabs Props

| 名称           | 类型                                                         | 默认值       | 说明                                                                                    |
| -------------- | ------------------------------------------------------------ | ------------ | --------------------------------------------------------------------------------------- |
| value(v-model) | `string`                                                     | `-`          | 绑定值，选中标签的 name                                                                 |
| default-value  | `string`                                                     | 第一个选项卡 | 初始化选中标签的 name                                                                   |
| justify        | `string`                                                     | `-`          | [flex 布局下主轴的排列方式](https://developer.mozilla.org/docs/Web/CSS/justify-content) |
| type           | `'bar'` `'line'` `'card'` `'border-card'` `'segment'`        | `'bar'`      | 标签类型                                                                                |
| before-leave   | `(name: string, old: string) => boolean \| Promise<boolean>` | `-`          | 切换标签之前的钩子函数                                                                  |

### TabPane Props

| 名称  | 类型             | 默认值 | 说明         |
| ----- | ---------------- | ------ | ------------ |
| name  | `string`         | `-`    | 标签的 `key` |
| label | `string \| slot` | `-`    | 标签的标题   |
