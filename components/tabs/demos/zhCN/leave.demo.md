# 切换 Tab 前的回调

试试延迟或阻止标签切换。

```html
<x-tabs type="segment" :before-leave="handleBeforeLeave">
  <x-tab-pane label="可以" name="okay">
    <x-tag size="large">分区</x-tag>
  </x-tab-pane>
  <x-tab-pane label="等 1 秒" name="wait">
    <x-tag size="large">个人中心</x-tag>
  </x-tab-pane>
  <x-tab-pane label="不许进来" name="not-allowed">
    <x-tag size="large">推荐</x-tag>
  </x-tab-pane>
</x-tabs>
```

```js
import { Notification, Message } from 'x-ui-vue3'

export default {
  setup() {
    return {
      handleBeforeLeave(name) {
        switch (name) {
          case 'okay':
            Notification.create({ title: name })
            return true
          case 'wait':
            return new Promise(resolve => {
              const hide = Message.loading('wait……', { duration: 0 })
              setTimeout(() => {
                hide()
                resolve(true)
              }, 1000)
            })
          case 'not-allowed':
            Notification.create({ title: '不许进来' })
            return false
          default:
            return true
        }
      }
    }
  }
}
```