# 基础用法

```html
<x-tabs>
  <x-tab-pane label="个人中心" name="1">
    <x-tag size="large">个人中心</x-tag>
  </x-tab-pane>
  <x-tab-pane label="推荐" name="2">
    <x-tag size="large">推荐</x-tag>
  </x-tab-pane>
  <x-tab-pane label="分区" name="3">
    <x-tag size="large">分区</x-tag>
  </x-tab-pane>
  <x-tab-pane label="动态" name="4">
    <x-tag size="large">动态</x-tag>
  </x-tab-pane>
  <x-tab-pane label="追番" name="5">
    <x-tag size="large">追番</x-tag>
  </x-tab-pane>
</x-tabs>
```