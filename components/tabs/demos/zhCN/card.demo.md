# 选项卡样式

```html
<x-tabs type="card" style="max-width: 600px;">
  <x-tab-pane label="光明的未来" name="1">
    <x-tag size="large" type="success">哥哥考上了公务员，妹妹接管家里生意，朋友创业发家致富，我当上了超市促销员：我们都有光明的未来!</x-tag>
  </x-tab-pane>
  <x-tab-pane label="请问您今天要来点兔子吗" name="2" display-directive="show">
    <x-tag size="large">请问您今天要来点兔子吗？？!</x-tag>
  </x-tab-pane>
  <x-tab-pane label="好耶！！！
" name="3">
    <x-tag size="large" type="success">禁止好耶！！！</x-tag>
  </x-tab-pane>
  <x-tab-pane label="禁止好耶！！！
" name="4">
    <x-tag size="large" type="success">好耶！！！</x-tag>
  </x-tab-pane>
</x-tabs>
```