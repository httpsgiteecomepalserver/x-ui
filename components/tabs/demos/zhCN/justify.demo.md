# Flex 布局的标签

使用 `justify` 定义标签布局

```html
<x-space vertical size="16">
  <x-card title="center">
    <x-tabs justify="center" type="line">
      <x-tab-pane label="个人中心" name="1">
        <x-tag size="large">个人中心</x-tag>
      </x-tab-pane>
      <x-tab-pane label="推荐" name="2">
        <x-tag size="large">推荐</x-tag>
      </x-tab-pane>
      <x-tab-pane label="分区" name="3">
        <x-tag size="large">分区</x-tag>
      </x-tab-pane>
      <x-tab-pane label="动态" name="4">
        <x-tag size="large">动态</x-tag>
      </x-tab-pane>
      <x-tab-pane label="追番" name="5">
        <x-tag size="large">追番</x-tag>
      </x-tab-pane>
    </x-tabs>
  </x-card>

  <x-card title="end">
    <x-tabs justify="end" type="line">
      <x-tab-pane label="个人中心" name="1">
        <x-tag size="large">个人中心</x-tag>
      </x-tab-pane>
      <x-tab-pane label="推荐" name="2">
        <x-tag size="large">推荐</x-tag>
      </x-tab-pane>
      <x-tab-pane label="分区" name="3">
        <x-tag size="large">分区</x-tag>
      </x-tab-pane>
      <x-tab-pane label="动态" name="4">
        <x-tag size="large">动态</x-tag>
      </x-tab-pane>
      <x-tab-pane label="追番" name="5">
        <x-tag size="large">追番</x-tag>
      </x-tab-pane>
    </x-tabs>
  </x-card>

  <x-card title="space-evenly">
    <x-tabs justify="space-evenly" type="border-card">
      <x-tab-pane label="个人中心" name="1">
        <x-tag size="large">个人中心</x-tag>
      </x-tab-pane>
      <x-tab-pane label="推荐" name="2">
        <x-tag size="large">推荐</x-tag>
      </x-tab-pane>
      <x-tab-pane label="分区" name="3">
        <x-tag size="large">分区</x-tag>
      </x-tab-pane>
      <x-tab-pane label="动态" name="4">
        <x-tag size="large">动态</x-tag>
      </x-tab-pane>
      <x-tab-pane label="追番" name="5">
        <x-tag size="large">追番</x-tag>
      </x-tab-pane>
    </x-tabs>
  </x-card>
</x-space>
```