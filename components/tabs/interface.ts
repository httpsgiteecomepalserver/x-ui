import { ComponentPublicInstance, InjectionKey, Ref } from 'vue'
import { TabPaneProps } from './TabPane'

export interface TabsContextProps {
  value: Ref<string | number | undefined>
  tabpanes: Ref<ComponentPublicInstance<TabPaneProps>[]>
}

export const TabsContextKey: InjectionKey<TabsContextProps> = Symbol('TabsContextKey')
