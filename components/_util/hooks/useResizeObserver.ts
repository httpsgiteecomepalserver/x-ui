import { getCurrentInstance, onBeforeUnmount, onMounted } from 'vue'
import resizeobserver from '../Dom/resizeobserver'
import { findDOMNode } from '../props-util'

export default function useResizeObserver(handler: (entry: ResizeObserverEntry) => void) {
  onMounted(() => {
    const el = findDOMNode(getCurrentInstance()) as Element
    resizeobserver.observer(el, handler)
  })
  onBeforeUnmount(() => {
    const el = findDOMNode(getCurrentInstance()) as Element
    resizeobserver.dispose(el)
  })
}