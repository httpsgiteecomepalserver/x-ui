import { Ref, ref, watch } from 'vue'

export default function <T>(getValue: () => T): Ref<T> {
  const cacheRef = ref()

  watch(getValue, val => (cacheRef.value = val), { immediate: true })

  return cacheRef
}
