export default function useNamespace(block: string) {
  const namespace = 'x'
  
  const b = () => block.includes(`${namespace}-`) ? block : `${namespace}-${block}`

  const m = (m: string) => `${b()}-${m}`
  const e = (e: string) => `${b()}_${e}`
  const em = (e: string, m: string) => `${b()}_${e}-${m}`
  const is = (name: string, state: boolean) => state ? `is-${name}` : ''

  return {
    b,
    m,
    e,
    em,
    is
  }
}