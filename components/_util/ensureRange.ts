const ensureRange = (val: number, min: number, max: number) => Math.max(min, Math.min(max, val))

export default ensureRange
