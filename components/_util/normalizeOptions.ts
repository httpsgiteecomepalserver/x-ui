import { isObject } from '@vue/shared'

type sn = string | number

export type Option = { label: sn; value: sn } & Record<string, any>
export type WillOption = sn | Partial<Option>

export function normalizeOption(option: WillOption) {
  return isObject(option) ? ((option.label ??= option.value), (option.value ??= option.label), option as Option) : { label: option, value: option }
}

export function normalizeOptions(options: WillOption[]) {
  return options.map(normalizeOption)
}

export function mapOptions<T>(options: WillOption[], cb: (opt: Option) => T) {
  return options.map(e => cb(normalizeOption(e)))
}
