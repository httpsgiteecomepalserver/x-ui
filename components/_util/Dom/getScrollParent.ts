export function getScrollParent(el: HTMLElement) {
  while (true) {
    el = el?.parentNode as HTMLElement
    if (!el) break
    if (el.nodeType === 9) return el // document
    const { overflow, overflowX, overflowY } = getComputedStyle(el);
    if (/(auto|scroll|overlay)/.test(overflow + overflowY + overflowX)) break
  }
  return el
}

export function getScrollParents(el: HTMLElement) {
  let sccrollableNodes: HTMLElement[] = []
  let cursor = el
  while (true) {
    cursor = getScrollParent(cursor) as HTMLElement
    if (!cursor) break
    sccrollableNodes.push(cursor)
  }
  return sccrollableNodes
}