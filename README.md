<h1 align="center">x-ui-vue3</h1>

<p align="center">
  <strong>一个 win10 风格的组件库</strong>
</p>

<div>
  <a href="http://httpsgiteecomepalserver.gitee.io/x-ui/zh-CN/components/form" target="_blank">
    <img class="preview-img" src="https://gitee.com/httpsgiteecomepalserver/x-ui/raw/master/images/form-base.png" />
  </a>
  <a href="http://httpsgiteecomepalserver.gitee.io/x-ui/zh-CN/components/calendar" target="_blank">
    <img class="preview-img" src="https://gitee.com/httpsgiteecomepalserver/x-ui/raw/master/images/calendar.png" />
  </a>
</div>

### 文档

[**这里**](http://httpsgiteecomepalserver.gitee.io/x-ui) 可以查看在线文档

### 支持环境

> 仅支持 Vue >= 3.2.0

| <img src="https://www.google.cn/chrome/static/ images/chrome-logo.svg" alt="Chrome" /><br/>Chrome |
| ------------------------------------------------------------------------------------------------- |
| 78+                                                                                               |

### 安装

```shell
# NPM
npm i -S x-ui-vue3 @vueuse/core moment
```

### 全量引入

```js
import { createApp } from 'vue'
import XUI from 'x-ui-vue3'
import App from './App.vue'
import 'x-ui-vue3/theme/light.css' // 浅色主题
// import 'x-ui-vue3/theme/dark.css' // 深色主题

const app = createApp(App)
app.use(XUI)
```

### vite 按需引入

```shell
npm install -D unplugin-vue-components
```

```js
// vite.config.ts
import { defineConfig } from 'vite'
import Components from 'unplugin-vue-components/vite'
import { XUIResolver } from 'x-ui-vue3/plugins'

export default defineConfig({
  // ...
  plugins: [
    Components({
      resolvers: [XUIResolver()]
    })
  ]
})
```

### 类型支持

```js
// tsconfig.json
{
  "compilerOptions": {
    // ...
    "types": ["x-ui-vue3/global"]
  }
}
```

## 常见问题

<!--  -->

1. `这个组件库为什么会存在？`
   - `因为好玩 🤣`
   - `喜欢 win10 风格，所以就尝试着做一套 win10 组件库`
2. `这个组件库有什么优点吗？`
   - `与 naive-ui、antdv 等主流UI库相比并没什么优势`
   - `唯一的优点就是 win10 风格`
3. `那有什么缺点吗？`
   - `😙亲！缺点可多了呢！`
   - `功能不丰富`
   - `文档不完善`
   - `可能有bug`
   - `只测试了 Chrome 最新版本，其他浏览器性兼容未知`
4. `我是编程新手，可以用吗？`
   - `不推荐`
   - `建议学习其他主流UI库，此 UI 仅供学习娱乐`

<br/>
<br/>
<br/>
<br/>
<br/>
