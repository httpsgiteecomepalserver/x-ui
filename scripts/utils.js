const fs = require('fs')

const targets = (exports.targets = fs.readdirSync('components').filter(f => {
  if (!fs.statSync(`components/${f}`).isDirectory()) {
    return false
  }
  const path = `components/${f}/package.json`
  if (!fs.existsSync(path)) return false
  const pkg = require('../' + path)
  if (pkg.private && !pkg.buildOptions) {
    return false
  }
  return true
}))