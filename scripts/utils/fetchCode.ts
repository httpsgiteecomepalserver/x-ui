export const reObj = {
  template: /<template[^>]*>([\s\S]*)<\/template>/,
  script: /<script[^>]*>([\s\S]*)<\/script>/,
  scriptContent: /(?<=<script[^>]*>)([\s\S]*)(?=<\/script>)/,
  markdown: /<markdown>([\s\S]*)<\/markdown>/,
  markdownContent: /(?<=<markdown>)([\s\S]*)(?=<\/markdown>)/,
  style: /<style[^>]*>([\s\S]*)<\/style>/,
};

export default function fetchCode(code: string, type: keyof typeof reObj): string {
  const matches = code.match(reObj[type]);
  return matches ? matches[0] : '';
}