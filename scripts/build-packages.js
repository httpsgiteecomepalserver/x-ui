const exec = require('util').promisify(require('child_process').exec)
const { targets } = require('./utils')

buildAll()

async function buildAll() {
  for (const e of targets) {
    await exec(`rollup -c --environment TARGET:${e}`)
  }
}