const fs = require('fs');
const path = require('path');

const distDir = path.join(process.cwd(), 'dist')

function finalizeCompile() {
  // Build a entry scss file to components/style
  const componentsPath = path.join(process.cwd(), 'components');
  let componentsLessContent = '';
  fs.readdir(componentsPath, (err, files) => {
    files.forEach(file => {
      if (fs.existsSync(path.join(componentsPath, file, 'style'))) {
        componentsLessContent += `@import "../${path.join(file, 'style')}";\n`;
      }
    });
    fs.writeFileSync(
      path.join(componentsPath, 'style', 'components.scss'),
      componentsLessContent,
    );
  });
}

function buildThemeFile(theme) {
  fs.writeFileSync(
    path.join(distDir, `${theme}.scss`),
    // `@import "../components/style/${theme}";\n@import "../components/style/components";`,
    `@import "../components/style/${theme}";`,
  )
}

function finalizeDist() {
  const dist = path.join(__dirname, '../dist')
  if (!fs.existsSync(dist)) fs.mkdirSync(dist)

  buildThemeFile('default');
  buildThemeFile('dark');
}

// finalizeCompile();
finalizeDist();