import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import Unocss from 'unocss/vite'
import Component from 'unplugin-vue-components/vite'
import { presetUno, presetAttributify } from 'unocss'
import cdnImport from 'vite-plugin-cdn-import'
import compressPlugin from 'vite-plugin-compression'
import { visualizer } from 'rollup-plugin-visualizer'
import md from './build/vite-plugin-md'
import docs from './build/plugins/docs/index'
import cdns from './cdns'
import path from 'path'
import { XUIResolver } from './plugins'
import dts from 'vite-plugin-dts'

const commonPlugins = [
  docs(),
  ...md(),
  Unocss({ presets: [presetUno()] })
  // Component({
  //   include: [/\.vue$/, /\.md$/],
  //   resolvers: [XUIResolver()]
  // })
]

const plugins =
  process.env.NODE_ENV === 'production'
    ? [
        ...commonPlugins,
        // cdnImport({
        //   modules: cdns.map(e => ({ name: e.module, var: e.global, path: e.entry }))
        // }),
        // compressPlugin(),
        visualizer({
          open: true,
          gzipSize: true,
          brotliSize: true
        })
      ]
    : commonPlugins

export default defineConfig({
  base: '/x-ui',
  plugins,
  resolve: {
    alias: [
      {
        find: 'x-ui-vue3',
        replacement: path.resolve(__dirname, 'components')
      }
    ],
    extensions: ['.vue', '.tsx', '.ts', '.mjs', '.js', '.jsx', '.json', 'md']
  },
  server: {
    host: '0.0.0.0'
  },
  ssgOptions: {
    format: 'cjs', // 使用 esm 时，build会报错：RevealEffect.install is not a function
    mock: true
  }
})
